//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/stha1de/jre_emul/junit/build_result/java/junit/framework/TestResult.java
//
//  Created by stha1de on 09.08.13.
//

#include "java/lang/ThreadDeath.h"
#include "java/lang/Throwable.h"
#include "java/util/ArrayList.h"
#include "java/util/Collections.h"
#include "java/util/Enumeration.h"
#include "java/util/Iterator.h"
#include "java/util/List.h"
#include "junit/framework/AssertionFailedError.h"
#include "junit/framework/Protectable.h"
#include "junit/framework/Test.h"
#include "junit/framework/TestCase.h"
#include "junit/framework/TestFailure.h"
#include "junit/framework/TestListener.h"
#include "junit/framework/TestResult.h"

@implementation JunitFrameworkTestResult

- (id<JavaUtilList>)fFailures {
  return fFailures_;
}
- (void)setFFailures:(id<JavaUtilList>)fFailures {
  JreOperatorRetainedAssign(&fFailures_, self, fFailures);
}
@synthesize fFailures = fFailures_;
- (id<JavaUtilList>)fErrors {
  return fErrors_;
}
- (void)setFErrors:(id<JavaUtilList>)fErrors {
  JreOperatorRetainedAssign(&fErrors_, self, fErrors);
}
@synthesize fErrors = fErrors_;
- (id<JavaUtilList>)fListeners {
  return fListeners_;
}
- (void)setFListeners:(id<JavaUtilList>)fListeners {
  JreOperatorRetainedAssign(&fListeners_, self, fListeners);
}
@synthesize fListeners = fListeners_;
@synthesize fRunTests = fRunTests_;
@synthesize fStop = fStop_;

- (id)init {
  if ((self = [super init])) {
    JreOperatorRetainedAssign(&fFailures_, self, [[[JavaUtilArrayList alloc] init] autorelease]);
    JreOperatorRetainedAssign(&fErrors_, self, [[[JavaUtilArrayList alloc] init] autorelease]);
    JreOperatorRetainedAssign(&fListeners_, self, [[[JavaUtilArrayList alloc] init] autorelease]);
    fRunTests_ = 0;
    fStop_ = NO;
  }
  return self;
}

- (void)addErrorWithJunitFrameworkTest:(id<JunitFrameworkTest>)test
                 withJavaLangThrowable:(JavaLangThrowable *)t {
  @synchronized(self) {
    {
      [((id<JavaUtilList>) nil_chk(fErrors_)) addWithId:[[[JunitFrameworkTestFailure alloc] initWithJunitFrameworkTest:test withJavaLangThrowable:t] autorelease]];
      {
        id<JavaUtilIterator> iter__ = [((id<JavaUtilList>) nil_chk([self cloneListeners])) iterator];
        while ([((id<JavaUtilIterator>) nil_chk(iter__)) hasNext]) {
          id<JunitFrameworkTestListener> each = [((id<JavaUtilIterator>) nil_chk(iter__)) next];
          [((id<JunitFrameworkTestListener>) nil_chk(each)) addErrorWithJunitFrameworkTest:test withJavaLangThrowable:t];
        }
      }
    }
  }
}

- (void)addFailureWithJunitFrameworkTest:(id<JunitFrameworkTest>)test
  withJunitFrameworkAssertionFailedError:(JunitFrameworkAssertionFailedError *)t {
  @synchronized(self) {
    {
      [((id<JavaUtilList>) nil_chk(fFailures_)) addWithId:[[[JunitFrameworkTestFailure alloc] initWithJunitFrameworkTest:test withJavaLangThrowable:t] autorelease]];
      {
        id<JavaUtilIterator> iter__ = [((id<JavaUtilList>) nil_chk([self cloneListeners])) iterator];
        while ([((id<JavaUtilIterator>) nil_chk(iter__)) hasNext]) {
          id<JunitFrameworkTestListener> each = [((id<JavaUtilIterator>) nil_chk(iter__)) next];
          [((id<JunitFrameworkTestListener>) nil_chk(each)) addFailureWithJunitFrameworkTest:test withJunitFrameworkAssertionFailedError:t];
        }
      }
    }
  }
}

- (void)addListenerWithJunitFrameworkTestListener:(id<JunitFrameworkTestListener>)listener {
  @synchronized(self) {
    {
      [((id<JavaUtilList>) nil_chk(fListeners_)) addWithId:listener];
    }
  }
}

- (void)removeListenerWithJunitFrameworkTestListener:(id<JunitFrameworkTestListener>)listener {
  @synchronized(self) {
    {
      [((id<JavaUtilList>) nil_chk(fListeners_)) removeWithId:listener];
    }
  }
}

- (id<JavaUtilList>)cloneListeners {
  @synchronized(self) {
    {
      id<JavaUtilList> result = [[[JavaUtilArrayList alloc] init] autorelease];
      [((id<JavaUtilList>) nil_chk(result)) addAllWithJavaUtilCollection:fListeners_];
      return result;
    }
  }
}

- (void)endTestWithJunitFrameworkTest:(id<JunitFrameworkTest>)test {
  {
    id<JavaUtilIterator> iter__ = [((id<JavaUtilList>) nil_chk([self cloneListeners])) iterator];
    while ([((id<JavaUtilIterator>) nil_chk(iter__)) hasNext]) {
      id<JunitFrameworkTestListener> each = [((id<JavaUtilIterator>) nil_chk(iter__)) next];
      [((id<JunitFrameworkTestListener>) nil_chk(each)) endTestWithJunitFrameworkTest:test];
    }
  }
}

- (int)errorCount {
  @synchronized(self) {
    {
      return [((id<JavaUtilList>) nil_chk(fErrors_)) size];
    }
  }
}

- (id<JavaUtilEnumeration>)errors {
  @synchronized(self) {
    {
      return [JavaUtilCollections enumerationWithJavaUtilCollection:fErrors_];
    }
  }
}

- (int)failureCount {
  @synchronized(self) {
    {
      return [((id<JavaUtilList>) nil_chk(fFailures_)) size];
    }
  }
}

- (id<JavaUtilEnumeration>)failures {
  @synchronized(self) {
    {
      return [JavaUtilCollections enumerationWithJavaUtilCollection:fFailures_];
    }
  }
}

- (void)runWithJunitFrameworkTestCase:(JunitFrameworkTestCase *)test {
  [self startTestWithJunitFrameworkTest:test];
  id<JunitFrameworkProtectable> p = [[[JunitFrameworkTestResult_$1 alloc] initWithJunitFrameworkTestCase:test] autorelease];
  [self runProtectedWithJunitFrameworkTest:test withJunitFrameworkProtectable:p];
  [self endTestWithJunitFrameworkTest:test];
}

- (int)runCount {
  @synchronized(self) {
    {
      return fRunTests_;
    }
  }
}

- (void)runProtectedWithJunitFrameworkTest:(id<JunitFrameworkTest>)test
             withJunitFrameworkProtectable:(id<JunitFrameworkProtectable>)p {
  @try {
    [((id<JunitFrameworkProtectable>) nil_chk(p)) protect];
  }
  @catch (JunitFrameworkAssertionFailedError *e) {
    [self addFailureWithJunitFrameworkTest:test withJunitFrameworkAssertionFailedError:e];
  }
  @catch (JavaLangThreadDeath *e) {
    @throw e;
  }
  @catch (JavaLangThrowable *e) {
    [self addErrorWithJunitFrameworkTest:test withJavaLangThrowable:e];
  }
}

- (BOOL)shouldStop {
  @synchronized(self) {
    {
      return fStop_;
    }
  }
}

- (void)startTestWithJunitFrameworkTest:(id<JunitFrameworkTest>)test {
  int count = [((id<JunitFrameworkTest>) nil_chk(test)) countTestCases];
  @synchronized (self) {
    fRunTests_ += count;
  }
  {
    id<JavaUtilIterator> iter__ = [((id<JavaUtilList>) nil_chk([self cloneListeners])) iterator];
    while ([((id<JavaUtilIterator>) nil_chk(iter__)) hasNext]) {
      id<JunitFrameworkTestListener> each = [((id<JavaUtilIterator>) nil_chk(iter__)) next];
      [((id<JunitFrameworkTestListener>) nil_chk(each)) startTestWithJunitFrameworkTest:test];
    }
  }
}

- (void)stop {
  @synchronized(self) {
    {
      fStop_ = YES;
    }
  }
}

- (BOOL)wasSuccessful {
  @synchronized(self) {
    {
      return [self failureCount] == 0 && [self errorCount] == 0;
    }
  }
}

- (void)dealloc {
  JreOperatorRetainedAssign(&fListeners_, self, nil);
  JreOperatorRetainedAssign(&fErrors_, self, nil);
  JreOperatorRetainedAssign(&fFailures_, self, nil);
  [super dealloc];
}

- (void)copyAllPropertiesTo:(id)copy {
  [super copyAllPropertiesTo:copy];
  JunitFrameworkTestResult *typedCopy = (JunitFrameworkTestResult *) copy;
  typedCopy.fFailures = fFailures_;
  typedCopy.fErrors = fErrors_;
  typedCopy.fListeners = fListeners_;
  typedCopy.fRunTests = fRunTests_;
  typedCopy.fStop = fStop_;
}

@end
@implementation JunitFrameworkTestResult_$1

- (JunitFrameworkTestCase *)val$test {
  return val$test_;
}
- (void)setVal$test:(JunitFrameworkTestCase *)val$test {
  JreOperatorRetainedAssign(&val$test_, self, val$test);
}
@synthesize val$test = val$test_;

- (void)protect {
  [((JunitFrameworkTestCase *) nil_chk(val$test_)) runBare];
}

- (id)initWithJunitFrameworkTestCase:(JunitFrameworkTestCase *)capture$0 {
  if ((self = [super init])) {
    JreOperatorRetainedAssign(&val$test_, self, capture$0);
  }
  return self;
}

- (void)dealloc {
  JreOperatorRetainedAssign(&val$test_, self, nil);
  [super dealloc];
}

- (void)copyAllPropertiesTo:(id)copy {
  [super copyAllPropertiesTo:copy];
  JunitFrameworkTestResult_$1 *typedCopy = (JunitFrameworkTestResult_$1 *) copy;
  typedCopy.val$test = val$test_;
}

@end
