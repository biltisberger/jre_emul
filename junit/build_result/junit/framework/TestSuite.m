//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/stha1de/jre_emul/junit/build_result/java/junit/framework/TestSuite.java
//
//  Created by stha1de on 09.08.13.
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "java/io/PrintWriter.h"
#include "java/io/StringWriter.h"
#include "java/lang/IllegalAccessException.h"
#include "java/lang/InstantiationException.h"
#include "java/lang/NoSuchMethodException.h"
#include "java/lang/Throwable.h"
#include "java/lang/Void.h"
#include "java/lang/reflect/Constructor.h"
#include "java/lang/reflect/InvocationTargetException.h"
#include "java/lang/reflect/Method.h"
#include "java/lang/reflect/Modifier.h"
#include "java/util/ArrayList.h"
#include "java/util/Enumeration.h"
#include "java/util/Iterator.h"
#include "java/util/List.h"
#include "java/util/Vector.h"
#include "junit/framework/Assert.h"
#include "junit/framework/Test.h"
#include "junit/framework/TestCase.h"
#include "junit/framework/TestResult.h"
#include "junit/framework/TestSuite.h"

@implementation JunitFrameworkTestSuite

- (NSString *)fName {
  return fName_;
}
- (void)setFName:(NSString *)fName {
  JreOperatorRetainedAssign(&fName_, self, fName);
}
@synthesize fName = fName_;
- (JavaUtilVector *)fTests {
  return fTests_;
}
- (void)setFTests:(JavaUtilVector *)fTests {
  JreOperatorRetainedAssign(&fTests_, self, fTests);
}
@synthesize fTests = fTests_;

+ (id<JunitFrameworkTest>)createTestWithIOSClass:(IOSClass *)theClass
                                    withNSString:(NSString *)name {
  JavaLangReflectConstructor *constructor;
  @try {
    constructor = [JunitFrameworkTestSuite getTestConstructorWithIOSClass:theClass];
  }
  @catch (JavaLangNoSuchMethodException *e) {
    return [JunitFrameworkTestSuite warningWithNSString:[NSString stringWithFormat:@"Class %@ has no public constructor TestCase(String name) or TestCase()", [((IOSClass *) nil_chk(theClass)) getName]]];
  }
  id test;
  @try {
    if ((int) [((IOSObjectArray *) nil_chk([((JavaLangReflectConstructor *) nil_chk(constructor)) getParameterTypes])) count] == 0) {
      test = [((JavaLangReflectConstructor *) nil_chk(constructor)) newInstanceWithNSObjectArray:[IOSObjectArray arrayWithLength:0 type:[IOSClass classWithClass:[NSObject class]]]];
      if ([test isKindOfClass:[JunitFrameworkTestCase class]]) [((JunitFrameworkTestCase *) test) setNameWithNSString:name];
    }
    else {
      test = [((JavaLangReflectConstructor *) nil_chk(constructor)) newInstanceWithNSObjectArray:[IOSObjectArray arrayWithObjects:(id[]){ name } count:1 type:[IOSClass classWithClass:[NSObject class]]]];
    }
  }
  @catch (JavaLangInstantiationException *e) {
    return ([JunitFrameworkTestSuite warningWithNSString:[NSString stringWithFormat:@"Cannot instantiate test case: %@ (%@)", name, [JunitFrameworkTestSuite exceptionToStringWithJavaLangThrowable:e]]]);
  }
  @catch (JavaLangReflectInvocationTargetException *e) {
    return ([JunitFrameworkTestSuite warningWithNSString:[NSString stringWithFormat:@"Exception in constructor: %@ (%@)", name, [JunitFrameworkTestSuite exceptionToStringWithJavaLangThrowable:[((JavaLangReflectInvocationTargetException *) nil_chk(e)) getTargetException]]]]);
  }
  @catch (JavaLangIllegalAccessException *e) {
    return ([JunitFrameworkTestSuite warningWithNSString:[NSString stringWithFormat:@"Cannot access test case: %@ (%@)", name, [JunitFrameworkTestSuite exceptionToStringWithJavaLangThrowable:e]]]);
  }
  return (id<JunitFrameworkTest>) test;
}

+ (JavaLangReflectConstructor *)getTestConstructorWithIOSClass:(IOSClass *)theClass {
  @try {
    return [((IOSClass *) nil_chk(theClass)) getConstructor:[IOSObjectArray arrayWithObjects:(id[]){ [IOSClass classWithClass:[NSString class]] } count:1 type:[IOSClass classWithClass:[IOSClass class]]]];
  }
  @catch (JavaLangNoSuchMethodException *e) {
  }
  return [((IOSClass *) nil_chk(theClass)) getConstructor:[IOSObjectArray arrayWithLength:0 type:[IOSClass classWithClass:[IOSClass class]]]];
}

+ (id<JunitFrameworkTest>)warningWithNSString:(NSString *)message {
  return [[[JunitFrameworkTestSuite_$1 alloc] initWithNSString:@"warning" withNSString:message] autorelease];
}

+ (NSString *)exceptionToStringWithJavaLangThrowable:(JavaLangThrowable *)t {
  JavaIoStringWriter *stringWriter = [[[JavaIoStringWriter alloc] init] autorelease];
  JavaIoPrintWriter *writer = [[[JavaIoPrintWriter alloc] initWithJavaIoWriter:stringWriter] autorelease];
  [((JavaLangThrowable *) nil_chk(t)) printStackTraceWithJavaIoPrintWriter:writer];
  return [((JavaIoStringWriter *) nil_chk(stringWriter)) description];
}

- (id)init {
  if ((self = [super init])) {
    JreOperatorRetainedAssign(&fTests_, self, [[[JavaUtilVector alloc] initWithInt:10] autorelease]);
  }
  return self;
}

- (id)initJunitFrameworkTestSuiteWithIOSClass:(IOSClass *)theClass {
  if ((self = [super init])) {
    JreOperatorRetainedAssign(&fTests_, self, [[[JavaUtilVector alloc] initWithInt:10] autorelease]);
    [self addTestsFromTestCaseWithIOSClass:theClass];
  }
  return self;
}

- (id)initWithIOSClass:(IOSClass *)theClass {
  return [self initJunitFrameworkTestSuiteWithIOSClass:theClass];
}

- (void)addTestsFromTestCaseWithIOSClass:(IOSClass *)theClass {
  JreOperatorRetainedAssign(&fName_, self, [((IOSClass *) nil_chk(theClass)) getName]);
  @try {
    [JunitFrameworkTestSuite getTestConstructorWithIOSClass:theClass];
  }
  @catch (JavaLangNoSuchMethodException *e) {
    [self addTestWithJunitFrameworkTest:[JunitFrameworkTestSuite warningWithNSString:[NSString stringWithFormat:@"Class %@ has no public constructor TestCase(String name) or TestCase()", [((IOSClass *) nil_chk(theClass)) getName]]]];
    return;
  }
  if (![JavaLangReflectModifier isPublicWithInt:[((IOSClass *) nil_chk(theClass)) getModifiers]]) {
    [self addTestWithJunitFrameworkTest:[JunitFrameworkTestSuite warningWithNSString:[NSString stringWithFormat:@"Class %@ is not public", [((IOSClass *) nil_chk(theClass)) getName]]]];
    return;
  }
  IOSClass *superClass = theClass;
  id<JavaUtilList> names = [[[JavaUtilArrayList alloc] init] autorelease];
  while ([[IOSClass classWithProtocol:@protocol(JunitFrameworkTest)] isAssignableFrom:superClass]) {
    {
      IOSObjectArray *a__ = [((IOSClass *) nil_chk(superClass)) getDeclaredMethods];
      int n__ = (int) [((IOSObjectArray *) nil_chk(a__)) count];
      for (int i__ = 0; i__ < n__; i__++) {
        JavaLangReflectMethod *each = [((IOSObjectArray *) nil_chk(a__)) objectAtIndex:i__];
        [self addTestMethodWithJavaLangReflectMethod:each withJavaUtilList:names withIOSClass:theClass];
      }
    }
    superClass = [((IOSClass *) nil_chk(superClass)) getSuperclass];
  }
  if ([((JavaUtilVector *) nil_chk(fTests_)) size] == 0) [self addTestWithJunitFrameworkTest:[JunitFrameworkTestSuite warningWithNSString:[NSString stringWithFormat:@"No tests found in %@", [((IOSClass *) nil_chk(theClass)) getName]]]];
}

- (id)initWithIOSClass:(IOSClass *)theClass
          withNSString:(NSString *)name {
  if ((self = [self initJunitFrameworkTestSuiteWithIOSClass:theClass])) {
    [self setNameWithNSString:name];
  }
  return self;
}

- (id)initWithNSString:(NSString *)name {
  if ((self = [super init])) {
    JreOperatorRetainedAssign(&fTests_, self, [[[JavaUtilVector alloc] initWithInt:10] autorelease]);
    [self setNameWithNSString:name];
  }
  return self;
}

- (id)initJunitFrameworkTestSuiteWithIOSClassArray:(IOSObjectArray *)classes {
  if ((self = [super init])) {
    JreOperatorRetainedAssign(&fTests_, self, [[[JavaUtilVector alloc] initWithInt:10] autorelease]);
    {
      IOSObjectArray *a__ = classes;
      int n__ = (int) [((IOSObjectArray *) nil_chk(a__)) count];
      for (int i__ = 0; i__ < n__; i__++) {
        IOSClass *each = [((IOSObjectArray *) nil_chk(a__)) objectAtIndex:i__];
        [self addTestWithJunitFrameworkTest:[self testCaseForClassWithIOSClass:each]];
      }
    }
  }
  return self;
}

- (id)initWithIOSClassArray:(IOSObjectArray *)classes {
  return [self initJunitFrameworkTestSuiteWithIOSClassArray:classes];
}

- (id<JunitFrameworkTest>)testCaseForClassWithIOSClass:(IOSClass *)each {
  if ([[IOSClass classWithClass:[JunitFrameworkTestCase class]] isAssignableFrom:each]) return [[[JunitFrameworkTestSuite alloc] initWithIOSClass:[((IOSClass *) nil_chk(each)) asSubclass:[IOSClass classWithClass:[JunitFrameworkTestCase class]]]] autorelease];
  else return [JunitFrameworkTestSuite warningWithNSString:[NSString stringWithFormat:@"%@ does not extend TestCase", [((IOSClass *) nil_chk(each)) getCanonicalName]]];
}

- (id)initWithIOSClassArray:(IOSObjectArray *)classes
               withNSString:(NSString *)name {
  if ((self = [self initJunitFrameworkTestSuiteWithIOSClassArray:classes])) {
    [self setNameWithNSString:name];
  }
  return self;
}

- (void)addTestWithJunitFrameworkTest:(id<JunitFrameworkTest>)test {
  [((JavaUtilVector *) nil_chk(fTests_)) addWithId:test];
}

- (void)addTestSuiteWithIOSClass:(IOSClass *)testClass {
  [self addTestWithJunitFrameworkTest:[[[JunitFrameworkTestSuite alloc] initWithIOSClass:testClass] autorelease]];
}

- (int)countTestCases {
  int count = 0;
  {
    id<JavaUtilIterator> iter__ = [((JavaUtilVector *) nil_chk(fTests_)) iterator];
    while ([((id<JavaUtilIterator>) nil_chk(iter__)) hasNext]) {
      id<JunitFrameworkTest> each = [((id<JavaUtilIterator>) nil_chk(iter__)) next];
      count += [((id<JunitFrameworkTest>) nil_chk(each)) countTestCases];
    }
  }
  return count;
}

- (NSString *)getName {
  return fName_;
}

- (void)runWithJunitFrameworkTestResult:(JunitFrameworkTestResult *)result {
  {
    id<JavaUtilIterator> iter__ = [((JavaUtilVector *) nil_chk(fTests_)) iterator];
    while ([((id<JavaUtilIterator>) nil_chk(iter__)) hasNext]) {
      id<JunitFrameworkTest> each = [((id<JavaUtilIterator>) nil_chk(iter__)) next];
      if ([((JunitFrameworkTestResult *) nil_chk(result)) shouldStop]) break;
      [self runTestWithJunitFrameworkTest:each withJunitFrameworkTestResult:result];
    }
  }
}

- (void)runTestWithJunitFrameworkTest:(id<JunitFrameworkTest>)test
         withJunitFrameworkTestResult:(JunitFrameworkTestResult *)result {
  [((id<JunitFrameworkTest>) nil_chk(test)) runWithJunitFrameworkTestResult:result];
}

- (void)setNameWithNSString:(NSString *)name {
  JreOperatorRetainedAssign(&fName_, self, name);
}

- (id<JunitFrameworkTest>)testAtWithInt:(int)index {
  return [((JavaUtilVector *) nil_chk(fTests_)) getWithInt:index];
}

- (int)testCount {
  return [((JavaUtilVector *) nil_chk(fTests_)) size];
}

- (id<JavaUtilEnumeration>)tests {
  return [((JavaUtilVector *) nil_chk(fTests_)) elements];
}

- (NSString *)description {
  if ([self getName] != nil) return [self getName];
  return [super description];
}

- (void)addTestMethodWithJavaLangReflectMethod:(JavaLangReflectMethod *)m
                              withJavaUtilList:(id<JavaUtilList>)names
                                  withIOSClass:(IOSClass *)theClass {
  NSString *name = [((JavaLangReflectMethod *) nil_chk(m)) getName];
  if ([((id<JavaUtilList>) nil_chk(names)) containsWithId:name]) return;
  if (![self isPublicTestMethodWithJavaLangReflectMethod:m]) {
    if ([self isTestMethodWithJavaLangReflectMethod:m]) [self addTestWithJunitFrameworkTest:[JunitFrameworkTestSuite warningWithNSString:[NSString stringWithFormat:@"Test method isn't public: %@(%@)", [((JavaLangReflectMethod *) nil_chk(m)) getName], [((IOSClass *) nil_chk(theClass)) getCanonicalName]]]];
    return;
  }
  [((id<JavaUtilList>) nil_chk(names)) addWithId:name];
  [self addTestWithJunitFrameworkTest:[JunitFrameworkTestSuite createTestWithIOSClass:theClass withNSString:name]];
}

- (BOOL)isPublicTestMethodWithJavaLangReflectMethod:(JavaLangReflectMethod *)m {
  return [self isTestMethodWithJavaLangReflectMethod:m] && [JavaLangReflectModifier isPublicWithInt:[((JavaLangReflectMethod *) nil_chk(m)) getModifiers]];
}

- (BOOL)isTestMethodWithJavaLangReflectMethod:(JavaLangReflectMethod *)m {
  return (int) [((IOSObjectArray *) nil_chk([((JavaLangReflectMethod *) nil_chk(m)) getParameterTypes])) count] == 0 && [((NSString *) nil_chk([((JavaLangReflectMethod *) nil_chk(m)) getName])) hasPrefix:@"test"] && [((IOSClass *) nil_chk([((JavaLangReflectMethod *) nil_chk(m)) getReturnType])) isEqual:[JavaLangVoid TYPE]];
}

- (void)dealloc {
  JreOperatorRetainedAssign(&fTests_, self, nil);
  JreOperatorRetainedAssign(&fName_, self, nil);
  [super dealloc];
}

- (void)copyAllPropertiesTo:(id)copy {
  [super copyAllPropertiesTo:copy];
  JunitFrameworkTestSuite *typedCopy = (JunitFrameworkTestSuite *) copy;
  typedCopy.fName = fName_;
  typedCopy.fTests = fTests_;
}

@end
@implementation JunitFrameworkTestSuite_$1

- (NSString *)val$message {
  return val$message_;
}
- (void)setVal$message:(NSString *)val$message {
  JreOperatorRetainedAssign(&val$message_, self, val$message);
}
@synthesize val$message = val$message_;

- (void)runTest {
  [JunitFrameworkAssert failWithNSString:val$message_];
}

- (id)initWithNSString:(NSString *)arg$0
          withNSString:(NSString *)capture$0 {
  if ((self = [super initWithNSString:arg$0])) {
    JreOperatorRetainedAssign(&val$message_, self, capture$0);
  }
  return self;
}

- (void)dealloc {
  JreOperatorRetainedAssign(&val$message_, self, nil);
  [super dealloc];
}

- (void)copyAllPropertiesTo:(id)copy {
  [super copyAllPropertiesTo:copy];
  JunitFrameworkTestSuite_$1 *typedCopy = (JunitFrameworkTestSuite_$1 *) copy;
  typedCopy.val$message = val$message_;
}

@end
