//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: apache_harmony/classlib/modules/nio/src/main/java/common/java/nio/ReadOnlyHeapByteBuffer.java
//
//  Created by stha1de on 09.08.13.
//

#include "IOSByteArray.h"
#include "java/nio/Buffer.h"
#include "java/nio/ByteBuffer.h"
#include "java/nio/ByteOrder.h"
#include "java/nio/HeapByteBuffer.h"
#include "java/nio/ReadOnlyBufferException.h"
#include "java/nio/ReadOnlyHeapByteBuffer.h"
#include "org/apache/harmony/luni/platform/Endianness.h"

@implementation JavaNioReadOnlyHeapByteBuffer

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  return result;
}

+ (JavaNioReadOnlyHeapByteBuffer *)copy__WithJavaNioHeapByteBuffer:(JavaNioHeapByteBuffer *)other
                                                           withInt:(int)markOfOther OBJC_METHOD_FAMILY_NONE {
  JavaNioReadOnlyHeapByteBuffer *buf = [[[JavaNioReadOnlyHeapByteBuffer alloc] initWithByteArray:((JavaNioHeapByteBuffer *) nil_chk(other)).backingArray withInt:[((JavaNioHeapByteBuffer *) nil_chk(other)) capacity] withInt:((JavaNioHeapByteBuffer *) nil_chk(other)).offset] autorelease];
  ((JavaNioReadOnlyHeapByteBuffer *) nil_chk(buf)).limit_ = [((JavaNioHeapByteBuffer *) nil_chk(other)) limit];
  ((JavaNioReadOnlyHeapByteBuffer *) nil_chk(buf)).position_ = [((JavaNioHeapByteBuffer *) nil_chk(other)) position];
  ((JavaNioReadOnlyHeapByteBuffer *) nil_chk(buf)).mark_ = markOfOther;
  [((JavaNioReadOnlyHeapByteBuffer *) nil_chk(buf)) orderWithJavaNioByteOrder:[((JavaNioHeapByteBuffer *) nil_chk(other)) order]];
  return buf;
}

- (id)initWithByteArray:(IOSByteArray *)backingArray
                withInt:(int)capacity
                withInt:(int)arrayOffset {
  return JreMemDebugAdd([super initWithByteArray:backingArray withInt:capacity withInt:arrayOffset]);
}

- (JavaNioByteBuffer *)asReadOnlyBuffer {
  return [JavaNioReadOnlyHeapByteBuffer copy__WithJavaNioHeapByteBuffer:self withInt:mark__];
}

- (JavaNioByteBuffer *)compact {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)duplicate {
  return [JavaNioReadOnlyHeapByteBuffer copy__WithJavaNioHeapByteBuffer:self withInt:mark__];
}

- (BOOL)isReadOnly {
  return YES;
}

- (IOSByteArray *)protectedArray {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (int)protectedArrayOffset {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (BOOL)protectedHasArray {
  return NO;
}

- (JavaNioByteBuffer *)putWithChar:(char)b {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putWithInt:(int)index
                         withChar:(char)b {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putWithByteArray:(IOSByteArray *)src
                                withInt:(int)off
                                withInt:(int)len {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putDoubleWithDouble:(double)value {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putDoubleWithInt:(int)index
                             withDouble:(double)value {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putFloatWithFloat:(float)value {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putFloatWithInt:(int)index
                             withFloat:(float)value {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putIntWithInt:(int)value {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putIntWithInt:(int)index
                             withInt:(int)value {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putLongWithInt:(int)index
                          withLongInt:(long long int)value {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putLongWithLongInt:(long long int)value {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putShortWithInt:(int)index
                          withShortInt:(short int)value {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putShortWithShortInt:(short int)value {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)putWithJavaNioByteBuffer:(JavaNioByteBuffer *)buf {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioByteBuffer *)slice {
  JavaNioReadOnlyHeapByteBuffer *slice = [[[JavaNioReadOnlyHeapByteBuffer alloc] initWithByteArray:backingArray_ withInt:[self remaining] withInt:offset_ + position__] autorelease];
  ((JavaNioReadOnlyHeapByteBuffer *) nil_chk(slice)).order_ = order__;
  return slice;
}

- (void)dealloc {
  JreMemDebugRemove(self);
  [super dealloc];
}

@end
