//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: apache_harmony/classlib/modules/nio/src/main/java/common/java/nio/ReadOnlyLongArrayBuffer.java
//
//  Created by stha1de on 09.08.13.
//

#include "IOSLongArray.h"
#include "java/nio/Buffer.h"
#include "java/nio/LongArrayBuffer.h"
#include "java/nio/LongBuffer.h"
#include "java/nio/ReadOnlyBufferException.h"
#include "java/nio/ReadOnlyLongArrayBuffer.h"

@implementation JavaNioReadOnlyLongArrayBuffer

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  return result;
}

+ (JavaNioReadOnlyLongArrayBuffer *)copy__WithJavaNioLongArrayBuffer:(JavaNioLongArrayBuffer *)other
                                                             withInt:(int)markOfOther OBJC_METHOD_FAMILY_NONE {
  JavaNioReadOnlyLongArrayBuffer *buf = [[[JavaNioReadOnlyLongArrayBuffer alloc] initWithInt:[((JavaNioLongArrayBuffer *) nil_chk(other)) capacity] withLongArray:((JavaNioLongArrayBuffer *) nil_chk(other)).backingArray withInt:((JavaNioLongArrayBuffer *) nil_chk(other)).offset] autorelease];
  ((JavaNioReadOnlyLongArrayBuffer *) nil_chk(buf)).limit_ = [((JavaNioLongArrayBuffer *) nil_chk(other)) limit];
  ((JavaNioReadOnlyLongArrayBuffer *) nil_chk(buf)).position_ = [((JavaNioLongArrayBuffer *) nil_chk(other)) position];
  ((JavaNioReadOnlyLongArrayBuffer *) nil_chk(buf)).mark_ = markOfOther;
  return buf;
}

- (id)initWithInt:(int)capacity
    withLongArray:(IOSLongArray *)backingArray
          withInt:(int)arrayOffset {
  return JreMemDebugAdd([super initWithInt:capacity withLongArray:backingArray withInt:arrayOffset]);
}

- (JavaNioLongBuffer *)asReadOnlyBuffer {
  return [self duplicate];
}

- (JavaNioLongBuffer *)compact {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioLongBuffer *)duplicate {
  return [JavaNioReadOnlyLongArrayBuffer copy__WithJavaNioLongArrayBuffer:self withInt:mark__];
}

- (BOOL)isReadOnly {
  return YES;
}

- (IOSLongArray *)protectedArray {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (int)protectedArrayOffset {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (BOOL)protectedHasArray {
  return NO;
}

- (JavaNioLongBuffer *)putWithLongInt:(long long int)c {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioLongBuffer *)putWithInt:(int)index
                      withLongInt:(long long int)c {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioLongBuffer *)putWithJavaNioLongBuffer:(JavaNioLongBuffer *)buf {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioLongBuffer *)putWithLongArray:(IOSLongArray *)src
                                withInt:(int)off
                                withInt:(int)len {
  @throw [[[JavaNioReadOnlyBufferException alloc] init] autorelease];
}

- (JavaNioLongBuffer *)slice {
  return [[[JavaNioReadOnlyLongArrayBuffer alloc] initWithInt:[self remaining] withLongArray:backingArray_ withInt:offset_ + position__] autorelease];
}

- (void)dealloc {
  JreMemDebugRemove(self);
  [super dealloc];
}

@end
