//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: apache_harmony/classlib/modules/math/src/main/java/java/math/Conversion.java
//
//  Created by stha1de on 09.08.13.
//

#include "IOSCharArray.h"
#include "IOSIntArray.h"
#include "java/lang/Character.h"
#include "java/lang/Double.h"
#include "java/lang/Integer.h"
#include "java/lang/Long.h"
#include "java/lang/Math.h"
#include "java/lang/StringBuilder.h"
#include "java/lang/System.h"
#include "java/math/BigInteger.h"
#include "java/math/BitLevel.h"
#include "java/math/Conversion.h"
#include "java/math/Division.h"

@implementation JavaMathConversion

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:JavaMathConversion_digitFitInInt_ name:@"JavaMathConversion_digitFitInInt_"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:JavaMathConversion_bigRadices_ name:@"JavaMathConversion_bigRadices_"]];
  return result;
}

static IOSIntArray * JavaMathConversion_digitFitInInt_;
static IOSIntArray * JavaMathConversion_bigRadices_;

+ (IOSIntArray *)digitFitInInt {
  return JavaMathConversion_digitFitInInt_;
}

+ (IOSIntArray *)bigRadices {
  return JavaMathConversion_bigRadices_;
}

- (id)init {
  return JreMemDebugAdd([super init]);
}

+ (NSString *)bigInteger2StringWithJavaMathBigInteger:(JavaMathBigInteger *)val
                                              withInt:(int)radix {
  int sign = ((JavaMathBigInteger *) nil_chk(val)).sign;
  int numberLength = ((JavaMathBigInteger *) nil_chk(val)).numberLength;
  IOSIntArray *digits = ((JavaMathBigInteger *) nil_chk(val)).digits;
  if (sign == 0) {
    return @"0";
  }
  if (numberLength == 1) {
    int highDigit = [((IOSIntArray *) nil_chk(digits)) intAtIndex:numberLength - 1];
    long long int v = highDigit & (long long) 0xFFFFFFFFLL;
    if (sign < 0) {
      v = -v;
    }
    return [JavaLangLong toStringWithLongInt:v withInt:radix];
  }
  if ((radix == 10) || (radix < JavaLangCharacter_MIN_RADIX) || (radix > JavaLangCharacter_MAX_RADIX)) {
    return [((JavaMathBigInteger *) nil_chk(val)) description];
  }
  double bitsForRadixDigit;
  bitsForRadixDigit = [JavaLangMath logWithDouble:radix] / [JavaLangMath logWithDouble:2];
  int resLengthInChars = (int) ([((JavaMathBigInteger *) nil_chk([((JavaMathBigInteger *) nil_chk(val)) abs])) bitLength] / bitsForRadixDigit + ((sign < 0) ? 1 : 0)) + 1;
  IOSCharArray *result = [IOSCharArray arrayWithLength:resLengthInChars];
  int currentChar = resLengthInChars;
  int resDigit;
  if (radix != 16) {
    IOSIntArray *temp = [IOSIntArray arrayWithLength:numberLength];
    [JavaLangSystem arraycopyWithId:digits withInt:0 withId:temp withInt:0 withInt:numberLength];
    int tempLen = numberLength;
    int charsPerInt = [((IOSIntArray *) nil_chk(JavaMathConversion_digitFitInInt_)) intAtIndex:radix];
    int i;
    int bigRadix = [((IOSIntArray *) nil_chk(JavaMathConversion_bigRadices_)) intAtIndex:radix - 2];
    while (YES) {
      resDigit = [JavaMathDivision divideArrayByIntWithIntArray:temp withIntArray:temp withInt:tempLen withInt:bigRadix];
      int previous = currentChar;
      do {
        (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = [JavaLangCharacter forDigitWithInt:resDigit % radix withInt:radix];
      }
      while (((resDigit /= radix) != 0) && (currentChar != 0));
      int delta = charsPerInt - previous + currentChar;
      for (i = 0; i < delta && currentChar > 0; i++) {
        (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '0';
      }
      for (i = tempLen - 1; (i > 0) && ([((IOSIntArray *) nil_chk(temp)) intAtIndex:i] == 0); i--) {
        ;
      }
      tempLen = i + 1;
      if ((tempLen == 1) && ([((IOSIntArray *) nil_chk(temp)) intAtIndex:0] == 0)) {
        break;
      }
    }
  }
  else {
    for (int i = 0; i < numberLength; i++) {
      for (int j = 0; (j < 8) && (currentChar > 0); j++) {
        resDigit = [((IOSIntArray *) nil_chk(digits)) intAtIndex:i] >> (j << 2) & (int) 0xf;
        (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = [JavaLangCharacter forDigitWithInt:resDigit withInt:16];
      }
    }
  }
  while ([((IOSCharArray *) nil_chk(result)) charAtIndex:currentChar] == '0') {
    currentChar++;
  }
  if (sign == -1) {
    (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '-';
  }
  return [NSString stringWithCharacters:result offset:currentChar length:resLengthInChars - currentChar];
}

+ (NSString *)toDecimalScaledStringWithJavaMathBigInteger:(JavaMathBigInteger *)val
                                                  withInt:(int)scale_ {
  int sign = ((JavaMathBigInteger *) nil_chk(val)).sign;
  int numberLength = ((JavaMathBigInteger *) nil_chk(val)).numberLength;
  IOSIntArray *digits = ((JavaMathBigInteger *) nil_chk(val)).digits;
  int resLengthInChars;
  int currentChar;
  IOSCharArray *result;
  if (sign == 0) {
    {
      JavaLangStringBuilder *result1;
      switch (scale_) {
        case 0:
        return @"0";
        case 1:
        return @"0.0";
        case 2:
        return @"0.00";
        case 3:
        return @"0.000";
        case 4:
        return @"0.0000";
        case 5:
        return @"0.00000";
        case 6:
        return @"0.000000";
        default:
        result1 = [[[JavaLangStringBuilder alloc] init] autorelease];
        if (scale_ < 0) {
          [((JavaLangStringBuilder *) nil_chk(result1)) appendWithNSString:@"0E+"];
        }
        else {
          [((JavaLangStringBuilder *) nil_chk(result1)) appendWithNSString:@"0E"];
        }
        [((JavaLangStringBuilder *) nil_chk(result1)) appendWithInt:-scale_];
        return [((JavaLangStringBuilder *) nil_chk(result1)) description];
      }
    }
  }
  resLengthInChars = numberLength * 10 + 1 + 7;
  result = [IOSCharArray arrayWithLength:resLengthInChars + 1];
  currentChar = resLengthInChars;
  if (numberLength == 1) {
    int highDigit = [((IOSIntArray *) nil_chk(digits)) intAtIndex:0];
    if (highDigit < 0) {
      long long int v = highDigit & (long long) 0xFFFFFFFFLL;
      do {
        long long int prev = v;
        v /= 10;
        (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = (unichar) ((int) 0x0030 + ((int) (prev - v * 10)));
      }
      while (v != 0);
    }
    else {
      int v = highDigit;
      do {
        int prev = v;
        v /= 10;
        (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = (unichar) ((int) 0x0030 + (prev - v * 10));
      }
      while (v != 0);
    }
  }
  else {
    IOSIntArray *temp = [IOSIntArray arrayWithLength:numberLength];
    int tempLen = numberLength;
    [JavaLangSystem arraycopyWithId:digits withInt:0 withId:temp withInt:0 withInt:tempLen];
    while (YES) {
      long long int result11 = 0;
      for (int i1 = tempLen - 1; i1 >= 0; i1--) {
        long long int temp1 = (result11 << 32) + ([((IOSIntArray *) nil_chk(temp)) intAtIndex:i1] & (long long) 0xFFFFFFFFLL);
        long long int res = [JavaMathConversion divideLongByBillionWithLongInt:temp1];
        (*[((IOSIntArray *) nil_chk(temp)) intRefAtIndex:i1]) = (int) res;
        result11 = (int) (res >> 32);
      }
      int resDigit = (int) result11;
      int previous = currentChar;
      do {
        (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = (unichar) ((int) 0x0030 + (resDigit % 10));
      }
      while (((resDigit /= 10) != 0) && (currentChar != 0));
      int delta = 9 - previous + currentChar;
      for (int i = 0; (i < delta) && (currentChar > 0); i++) {
        (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '0';
      }
      int j = tempLen - 1;
      for (; [((IOSIntArray *) nil_chk(temp)) intAtIndex:j] == 0; j--) {
        if (j == 0) {
          goto break_BIG_LOOP;
        }
      }
      tempLen = j + 1;
    }
    break_BIG_LOOP: ;
    while ([((IOSCharArray *) nil_chk(result)) charAtIndex:currentChar] == '0') {
      currentChar++;
    }
  }
  BOOL negNumber = (sign < 0);
  int exponent = resLengthInChars - currentChar - scale_ - 1;
  if (scale_ == 0) {
    if (negNumber) {
      (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '-';
    }
    return [NSString stringWithCharacters:result offset:currentChar length:resLengthInChars - currentChar];
  }
  if ((scale_ > 0) && (exponent >= -6)) {
    if (exponent >= 0) {
      int insertPoint = currentChar + exponent;
      for (int j = resLengthInChars - 1; j >= insertPoint; j--) {
        (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:j + 1]) = [((IOSCharArray *) nil_chk(result)) charAtIndex:j];
      }
      (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:++insertPoint]) = '.';
      if (negNumber) {
        (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '-';
      }
      return [NSString stringWithCharacters:result offset:currentChar length:resLengthInChars - currentChar + 1];
    }
    for (int j = 2; j < -exponent + 1; j++) {
      (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '0';
    }
    (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '.';
    (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '0';
    if (negNumber) {
      (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '-';
    }
    return [NSString stringWithCharacters:result offset:currentChar length:resLengthInChars - currentChar];
  }
  int startPoint = currentChar + 1;
  int endPoint = resLengthInChars;
  JavaLangStringBuilder *result1 = [[[JavaLangStringBuilder alloc] initWithInt:16 + endPoint - startPoint] autorelease];
  if (negNumber) {
    [((JavaLangStringBuilder *) nil_chk(result1)) appendWithUnichar:'-'];
  }
  if (endPoint - startPoint >= 1) {
    [((JavaLangStringBuilder *) nil_chk(result1)) appendWithUnichar:[((IOSCharArray *) nil_chk(result)) charAtIndex:currentChar]];
    [((JavaLangStringBuilder *) nil_chk(result1)) appendWithUnichar:'.'];
    [((JavaLangStringBuilder *) nil_chk(result1)) appendWithCharArray:result withInt:currentChar + 1 withInt:resLengthInChars - currentChar - 1];
  }
  else {
    [((JavaLangStringBuilder *) nil_chk(result1)) appendWithCharArray:result withInt:currentChar withInt:resLengthInChars - currentChar];
  }
  [((JavaLangStringBuilder *) nil_chk(result1)) appendWithUnichar:'E'];
  if (exponent > 0) {
    [((JavaLangStringBuilder *) nil_chk(result1)) appendWithUnichar:'+'];
  }
  [((JavaLangStringBuilder *) nil_chk(result1)) appendWithNSString:[JavaLangInteger toStringWithInt:exponent]];
  return [((JavaLangStringBuilder *) nil_chk(result1)) description];
}

+ (NSString *)toDecimalScaledStringWithLongInt:(long long int)value
                                       withInt:(int)scale_ {
  int resLengthInChars;
  int currentChar;
  IOSCharArray *result;
  BOOL negNumber = value < 0;
  if (negNumber) {
    value = -value;
  }
  if (value == 0) {
    {
      JavaLangStringBuilder *result1;
      switch (scale_) {
        case 0:
        return @"0";
        case 1:
        return @"0.0";
        case 2:
        return @"0.00";
        case 3:
        return @"0.000";
        case 4:
        return @"0.0000";
        case 5:
        return @"0.00000";
        case 6:
        return @"0.000000";
        default:
        result1 = [[[JavaLangStringBuilder alloc] init] autorelease];
        if (scale_ < 0) {
          [((JavaLangStringBuilder *) nil_chk(result1)) appendWithNSString:@"0E+"];
        }
        else {
          [((JavaLangStringBuilder *) nil_chk(result1)) appendWithNSString:@"0E"];
        }
        [((JavaLangStringBuilder *) nil_chk(result1)) appendWithNSString:(scale_ == JavaLangInteger_MIN_VALUE) ? @"2147483648" : [JavaLangInteger toStringWithInt:-scale_]];
        return [((JavaLangStringBuilder *) nil_chk(result1)) description];
      }
    }
  }
  resLengthInChars = 18;
  result = [IOSCharArray arrayWithLength:resLengthInChars + 1];
  currentChar = resLengthInChars;
  long long int v = value;
  do {
    long long int prev = v;
    v /= 10;
    (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = (unichar) ((int) 0x0030 + (prev - v * 10));
  }
  while (v != 0);
  long long int exponent = (long long int) resLengthInChars - (long long int) currentChar - scale_ - 1LL;
  if (scale_ == 0) {
    if (negNumber) {
      (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '-';
    }
    return [NSString stringWithCharacters:result offset:currentChar length:resLengthInChars - currentChar];
  }
  if (scale_ > 0 && exponent >= -6) {
    if (exponent >= 0) {
      int insertPoint = currentChar + (int) exponent;
      for (int j = resLengthInChars - 1; j >= insertPoint; j--) {
        (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:j + 1]) = [((IOSCharArray *) nil_chk(result)) charAtIndex:j];
      }
      (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:++insertPoint]) = '.';
      if (negNumber) {
        (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '-';
      }
      return [NSString stringWithCharacters:result offset:currentChar length:resLengthInChars - currentChar + 1];
    }
    for (int j = 2; j < -exponent + 1; j++) {
      (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '0';
    }
    (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '.';
    (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '0';
    if (negNumber) {
      (*[((IOSCharArray *) nil_chk(result)) charRefAtIndex:--currentChar]) = '-';
    }
    return [NSString stringWithCharacters:result offset:currentChar length:resLengthInChars - currentChar];
  }
  int startPoint = currentChar + 1;
  int endPoint = resLengthInChars;
  JavaLangStringBuilder *result1 = [[[JavaLangStringBuilder alloc] initWithInt:16 + endPoint - startPoint] autorelease];
  if (negNumber) {
    [((JavaLangStringBuilder *) nil_chk(result1)) appendWithUnichar:'-'];
  }
  if (endPoint - startPoint >= 1) {
    [((JavaLangStringBuilder *) nil_chk(result1)) appendWithUnichar:[((IOSCharArray *) nil_chk(result)) charAtIndex:currentChar]];
    [((JavaLangStringBuilder *) nil_chk(result1)) appendWithUnichar:'.'];
    [((JavaLangStringBuilder *) nil_chk(result1)) appendWithCharArray:result withInt:currentChar + 1 withInt:resLengthInChars - currentChar - 1];
  }
  else {
    [((JavaLangStringBuilder *) nil_chk(result1)) appendWithCharArray:result withInt:currentChar withInt:resLengthInChars - currentChar];
  }
  [((JavaLangStringBuilder *) nil_chk(result1)) appendWithUnichar:'E'];
  if (exponent > 0) {
    [((JavaLangStringBuilder *) nil_chk(result1)) appendWithUnichar:'+'];
  }
  [((JavaLangStringBuilder *) nil_chk(result1)) appendWithNSString:[JavaLangLong toStringWithLongInt:exponent]];
  return [((JavaLangStringBuilder *) nil_chk(result1)) description];
}

+ (long long int)divideLongByBillionWithLongInt:(long long int)a {
  long long int quot;
  long long int rem;
  if (a >= 0) {
    long long int bLong = 1000000000LL;
    quot = (a / bLong);
    rem = (a % bLong);
  }
  else {
    long long int aPos = (long long) (((unsigned long long) a) >> 1);
    long long int bPos = (long long) (((unsigned long long) 1000000000LL) >> 1);
    quot = aPos / bPos;
    rem = aPos % bPos;
    rem = (rem << 1) + (a & 1);
  }
  return ((rem << 32) | (quot & (long long) 0xFFFFFFFFLL));
}

+ (double)bigInteger2DoubleWithJavaMathBigInteger:(JavaMathBigInteger *)val {
  if ((((JavaMathBigInteger *) nil_chk(val)).numberLength < 2) || ((((JavaMathBigInteger *) nil_chk(val)).numberLength == 2) && ([((IOSIntArray *) nil_chk(((JavaMathBigInteger *) nil_chk(val)).digits)) intAtIndex:1] > 0))) {
    return [((JavaMathBigInteger *) nil_chk(val)) longValue];
  }
  if (((JavaMathBigInteger *) nil_chk(val)).numberLength > 32) {
    return ((((JavaMathBigInteger *) nil_chk(val)).sign > 0) ? JavaLangDouble_POSITIVE_INFINITY : JavaLangDouble_NEGATIVE_INFINITY);
  }
  int bitLen = [((JavaMathBigInteger *) nil_chk([((JavaMathBigInteger *) nil_chk(val)) abs])) bitLength];
  long long int exponent = bitLen - 1;
  int delta = bitLen - 54;
  long long int lVal = [((JavaMathBigInteger *) nil_chk([((JavaMathBigInteger *) nil_chk([((JavaMathBigInteger *) nil_chk(val)) abs])) shiftRightWithInt:delta])) longValue];
  long long int mantissa = lVal & (long long) 0x1FFFFFFFFFFFFFLL;
  if (exponent == 1023) {
    if (mantissa == 0X1FFFFFFFFFFFFFLL) {
      return ((((JavaMathBigInteger *) nil_chk(val)).sign > 0) ? JavaLangDouble_POSITIVE_INFINITY : JavaLangDouble_NEGATIVE_INFINITY);
    }
    if (mantissa == (long long) 0x1FFFFFFFFFFFFELL) {
      return ((((JavaMathBigInteger *) nil_chk(val)).sign > 0) ? JavaLangDouble_MAX_VALUE : -JavaLangDouble_MAX_VALUE);
    }
  }
  if (((mantissa & 1) == 1) && (((mantissa & 2) == 2) || [JavaMathBitLevel nonZeroDroppedBitsWithInt:delta withIntArray:((JavaMathBigInteger *) nil_chk(val)).digits])) {
    mantissa += 2;
  }
  mantissa >>= 1;
  long long int resSign = (((JavaMathBigInteger *) nil_chk(val)).sign < 0) ? -0x7fffffffffffffffLL - 1 : 0;
  exponent = ((1023 + exponent) << 52) & (long long) 0x7FF0000000000000LL;
  long long int result = resSign | exponent | mantissa;
  return [JavaLangDouble longBitsToDoubleWithLongInt:result];
}

+ (void)initialize {
  if (self == [JavaMathConversion class]) {
    JreOperatorRetainedAssign(&JavaMathConversion_digitFitInInt_, self, [IOSIntArray arrayWithInts:(int[]){ -1, -1, 31, 19, 15, 13, 11, 11, 10, 9, 9, 8, 8, 8, 8, 7, 7, 7, 7, 7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5 } count:37]);
    JreOperatorRetainedAssign(&JavaMathConversion_bigRadices_, self, [IOSIntArray arrayWithInts:(int[]){ -0x7fffffff - 1, 1162261467, 1073741824, 1220703125, 362797056, 1977326743, 1073741824, 387420489, 1000000000, 214358881, 429981696, 815730721, 1475789056, 170859375, 268435456, 410338673, 612220032, 893871739, 1280000000, 1801088541, 113379904, 148035889, 191102976, 244140625, 308915776, 387420489, 481890304, 594823321, 729000000, 887503681, 1073741824, 1291467969, 1544804416, 1838265625, 60466176 } count:35]);
  }
}

- (void)dealloc {
  JreMemDebugRemove(self);
  [super dealloc];
}

@end
