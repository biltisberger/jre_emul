//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: apache_harmony/classlib/modules/luni/src/main/java/java/util/AbstractSet.java
//
//  Created by stha1de on 09.08.13.
//

#include "java/lang/ClassCastException.h"
#include "java/lang/NullPointerException.h"
#include "java/util/AbstractCollection.h"
#include "java/util/AbstractSet.h"
#include "java/util/Collection.h"
#include "java/util/Iterator.h"
#include "java/util/Set.h"

@implementation JavaUtilAbstractSet

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  return result;
}

- (id)init {
  return JreMemDebugAdd([super init]);
}

- (BOOL)isEqual:(id)object {
  if (self == object) {
    return YES;
  }
  if ([object conformsToProtocol: @protocol(JavaUtilSet)]) {
    id<JavaUtilSet> s = (id<JavaUtilSet>) object;
    @try {
      return [self size] == [((id<JavaUtilSet>) nil_chk(s)) size] && [self containsAllWithJavaUtilCollection:s];
    }
    @catch (JavaLangNullPointerException *ignored) {
      return NO;
    }
    @catch (JavaLangClassCastException *ignored) {
      return NO;
    }
  }
  return NO;
}

- (NSUInteger)hash {
  int result = 0;
  id<JavaUtilIterator> it = [self iterator];
  while ([((id<JavaUtilIterator>) nil_chk(it)) hasNext]) {
    id next = [((id<JavaUtilIterator>) nil_chk(it)) next];
    result += next == nil ? 0 : [nil_chk(next) hash];
  }
  return result;
}

- (BOOL)removeAllWithJavaUtilCollection:(id<JavaUtilCollection>)collection {
  BOOL result = NO;
  if ([self size] <= [((id<JavaUtilCollection>) nil_chk(collection)) size]) {
    id<JavaUtilIterator> it = [self iterator];
    while ([((id<JavaUtilIterator>) nil_chk(it)) hasNext]) {
      if ([((id<JavaUtilCollection>) nil_chk(collection)) containsWithId:[((id<JavaUtilIterator>) nil_chk(it)) next]]) {
        [((id<JavaUtilIterator>) nil_chk(it)) remove];
        result = YES;
      }
    }
  }
  else {
    id<JavaUtilIterator> it = [((id<JavaUtilCollection>) nil_chk(collection)) iterator];
    while ([((id<JavaUtilIterator>) nil_chk(it)) hasNext]) {
      result = [self removeWithId:[((id<JavaUtilIterator>) nil_chk(it)) next]] || result;
    }
  }
  return result;
}

- (void)dealloc {
  JreMemDebugRemove(self);
  [super dealloc];
}

@end
