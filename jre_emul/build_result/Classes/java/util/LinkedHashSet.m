//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: apache_harmony/classlib/modules/luni/src/main/java/java/util/LinkedHashSet.java
//
//  Created by stha1de on 09.08.13.
//

#include "java/util/Collection.h"
#include "java/util/HashMap.h"
#include "java/util/HashSet.h"
#include "java/util/Iterator.h"
#include "java/util/LinkedHashMap.h"
#include "java/util/LinkedHashSet.h"

@implementation JavaUtilLinkedHashSet

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  return result;
}

- (id)init {
  return JreMemDebugAdd([super initWithJavaUtilHashMap:[[[JavaUtilLinkedHashMap alloc] init] autorelease]]);
}

- (id)initWithInt:(int)capacity {
  return JreMemDebugAdd([super initWithJavaUtilHashMap:[[[JavaUtilLinkedHashMap alloc] initWithInt:capacity] autorelease]]);
}

- (id)initWithInt:(int)capacity
        withFloat:(float)loadFactor {
  return JreMemDebugAdd([super initWithJavaUtilHashMap:[[[JavaUtilLinkedHashMap alloc] initWithInt:capacity withFloat:loadFactor] autorelease]]);
}

- (id)initWithJavaUtilCollection:(id<JavaUtilCollection>)collection {
  if ((self = [super initWithJavaUtilHashMap:[[[JavaUtilLinkedHashMap alloc] initWithInt:[((id<JavaUtilCollection>) nil_chk(collection)) size] < 6 ? 11 : [((id<JavaUtilCollection>) nil_chk(collection)) size] * 2] autorelease]])) {
    {
      id<JavaUtilIterator> iter__ = [((id<JavaUtilCollection>) nil_chk(collection)) iterator];
      while ([((id<JavaUtilIterator>) nil_chk(iter__)) hasNext]) {
        id e = [((id<JavaUtilIterator>) nil_chk(iter__)) next];
        [self addWithId:e];
      }
    }
    JreMemDebugAdd(self);
  }
  return self;
}

- (JavaUtilHashMap *)createBackingMapWithInt:(int)capacity
                                   withFloat:(float)loadFactor {
  return [[[JavaUtilLinkedHashMap alloc] initWithInt:capacity withFloat:loadFactor] autorelease];
}

- (void)dealloc {
  JreMemDebugRemove(self);
  [super dealloc];
}

@end
