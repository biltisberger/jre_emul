//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: android/libcore/luni/src/main/java/java/util/Properties.java
//
//  Created by stha1de on 09.08.13.
//

#include "IOSCharArray.h"
#include "java/io/BufferedReader.h"
#include "java/io/IOException.h"
#include "java/io/InputStream.h"
#include "java/io/InputStreamReader.h"
#include "java/io/OutputStream.h"
#include "java/io/OutputStreamWriter.h"
#include "java/io/PrintStream.h"
#include "java/io/PrintWriter.h"
#include "java/io/Reader.h"
#include "java/io/Writer.h"
#include "java/lang/Appendable.h"
#include "java/lang/AssertionError.h"
#include "java/lang/Character.h"
#include "java/lang/IllegalArgumentException.h"
#include "java/lang/Integer.h"
#include "java/lang/NullPointerException.h"
#include "java/lang/StringBuilder.h"
#include "java/lang/System.h"
#include "java/util/Collections.h"
#include "java/util/Date.h"
#include "java/util/Enumeration.h"
#include "java/util/Hashtable.h"
#include "java/util/InvalidPropertiesFormatException.h"
#include "java/util/Iterator.h"
#include "java/util/Map.h"
#include "java/util/Properties.h"
#include "java/util/Set.h"
#include "org/xml/sax/Attributes.h"
#include "org/xml/sax/InputSource.h"
#include "org/xml/sax/SAXException.h"
#include "org/xml/sax/XMLReader.h"
#include "org/xml/sax/helpers/XMLReaderFactory.h"

@implementation JavaUtilProperties

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:JavaUtilProperties_PROP_DTD_NAME_ name:@"JavaUtilProperties_PROP_DTD_NAME_"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:JavaUtilProperties_PROP_DTD_ name:@"JavaUtilProperties_PROP_DTD_"]];
  return result;
}

static NSString * JavaUtilProperties_PROP_DTD_NAME_ = @"http://java.sun.com/dtd/properties.dtd";
static NSString * JavaUtilProperties_PROP_DTD_ = @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>    <!ELEMENT properties (comment?, entry*) >    <!ATTLIST properties version CDATA #FIXED \"1.0\" >    <!ELEMENT comment (#PCDATA) >    <!ELEMENT entry (#PCDATA) >    <!ATTLIST entry key CDATA #REQUIRED >";

- (JavaUtilProperties *)defaults {
  return defaults_;
}
- (void)setDefaults:(JavaUtilProperties *)defaults {
  JreOperatorRetainedAssign(&defaults_, self, defaults);
}
@synthesize defaults = defaults_;

+ (NSString *)PROP_DTD_NAME {
  return JavaUtilProperties_PROP_DTD_NAME_;
}

+ (NSString *)PROP_DTD {
  return JavaUtilProperties_PROP_DTD_;
}

- (id)init {
  return JreMemDebugAdd([super init]);
}

- (id)initWithJavaUtilProperties:(JavaUtilProperties *)properties {
  if ((self = [super init])) {
    JreOperatorRetainedAssign(&defaults_, self, properties);
    JreMemDebugAdd(self);
  }
  return self;
}

- (void)dumpStringWithJavaLangStringBuilder:(JavaLangStringBuilder *)buffer
                               withNSString:(NSString *)string
                                   withBOOL:(BOOL)key {
  int i = 0;
  if (!key && i < [((NSString *) nil_chk(string)) length] && [((NSString *) nil_chk(string)) charAtWithInt:i] == ' ') {
    [((JavaLangStringBuilder *) nil_chk(buffer)) appendWithNSString:@"\\ "];
    i++;
  }
  for (; i < [((NSString *) nil_chk(string)) length]; i++) {
    unichar ch = [((NSString *) nil_chk(string)) charAtWithInt:i];
    switch (ch) {
      case 0x0009:
      [((JavaLangStringBuilder *) nil_chk(buffer)) appendWithNSString:@"\\t"];
      break;
      case 0x000a:
      [((JavaLangStringBuilder *) nil_chk(buffer)) appendWithNSString:@"\\n"];
      break;
      case 0x000c:
      [((JavaLangStringBuilder *) nil_chk(buffer)) appendWithNSString:@"\\f"];
      break;
      case 0x000d:
      [((JavaLangStringBuilder *) nil_chk(buffer)) appendWithNSString:@"\\r"];
      break;
      default:
      if ([@"\\#!=:" indexOf:ch] >= 0 || (key && ch == ' ')) {
        [((JavaLangStringBuilder *) nil_chk(buffer)) appendWithUnichar:'\\'];
      }
      if (ch >= ' ' && ch <= '~') {
        [((JavaLangStringBuilder *) nil_chk(buffer)) appendWithUnichar:ch];
      }
      else {
        NSString *hex = [JavaLangInteger toHexStringWithInt:ch];
        [((JavaLangStringBuilder *) nil_chk(buffer)) appendWithNSString:@"\\u"];
        for (int j = 0; j < 4 - [((NSString *) nil_chk(hex)) length]; j++) {
          [((JavaLangStringBuilder *) nil_chk(buffer)) appendWithNSString:@"0"];
        }
        [((JavaLangStringBuilder *) nil_chk(buffer)) appendWithNSString:hex];
      }
    }
  }
}

- (NSString *)getPropertyWithNSString:(NSString *)name {
  id result = [super getWithId:name];
  NSString *property = [result isKindOfClass:[NSString class]] ? (NSString *) result : nil;
  if (property == nil && defaults_ != nil) {
    property = [((JavaUtilProperties *) nil_chk(defaults_)) getPropertyWithNSString:name];
  }
  return property;
}

- (NSString *)getPropertyWithNSString:(NSString *)name
                         withNSString:(NSString *)defaultValue {
  id result = [super getWithId:name];
  NSString *property = [result isKindOfClass:[NSString class]] ? (NSString *) result : nil;
  if (property == nil && defaults_ != nil) {
    property = [((JavaUtilProperties *) nil_chk(defaults_)) getPropertyWithNSString:name];
  }
  if (property == nil) {
    return defaultValue;
  }
  return property;
}

- (void)listWithJavaIoPrintStream:(JavaIoPrintStream *)outArg {
  [self listToAppendableWithJavaLangAppendable:outArg];
}

- (void)listWithJavaIoPrintWriter:(JavaIoPrintWriter *)outArg {
  [self listToAppendableWithJavaLangAppendable:outArg];
}

- (void)listToAppendableWithJavaLangAppendable:(id<JavaLangAppendable>)outArg {
  @try {
    if (outArg == nil) {
      @throw [[[JavaLangNullPointerException alloc] initWithNSString:@"out == null"] autorelease];
    }
    JavaLangStringBuilder *sb = [[[JavaLangStringBuilder alloc] initWithInt:80] autorelease];
    id<JavaUtilEnumeration> keys = [self propertyNames];
    while ([((id<JavaUtilEnumeration>) nil_chk(keys)) hasMoreElements]) {
      NSString *key = (NSString *) [((id<JavaUtilEnumeration>) nil_chk(keys)) nextElement];
      [((JavaLangStringBuilder *) nil_chk(sb)) appendWithNSString:key];
      [((JavaLangStringBuilder *) nil_chk(sb)) appendWithUnichar:'='];
      NSString *property = (NSString *) [super getWithId:key];
      JavaUtilProperties *def = defaults_;
      while (property == nil) {
        property = (NSString *) [((JavaUtilProperties *) nil_chk(def)) getWithId:key];
        def = ((JavaUtilProperties *) nil_chk(def)).defaults;
      }
      if ([((NSString *) nil_chk(property)) length] > 40) {
        [((JavaLangStringBuilder *) nil_chk(sb)) appendWithNSString:[((NSString *) nil_chk(property)) substring:0 endIndex:37]];
        [((JavaLangStringBuilder *) nil_chk(sb)) appendWithNSString:@"..."];
      }
      else {
        [((JavaLangStringBuilder *) nil_chk(sb)) appendWithNSString:property];
      }
      [((JavaLangStringBuilder *) nil_chk(sb)) appendWithNSString:[JavaLangSystem lineSeparator]];
      [((id<JavaLangAppendable>) nil_chk(outArg)) appendWithJavaLangCharSequence:[((JavaLangStringBuilder *) nil_chk(sb)) description]];
      [((JavaLangStringBuilder *) nil_chk(sb)) setLengthWithInt:0];
    }
  }
  @catch (JavaIoIOException *ex) {
    @throw [[[JavaLangAssertionError alloc] initWithId:ex] autorelease];
  }
}

- (void)load__WithJavaIoInputStream:(JavaIoInputStream *)inArg {
  @synchronized(self) {
    {
      if (inArg == nil) {
        @throw [[[JavaLangNullPointerException alloc] initWithNSString:@"in == null"] autorelease];
      }
      [self load__WithJavaIoReader:[[[JavaIoInputStreamReader alloc] initWithJavaIoInputStream:inArg withNSString:@"ISO-8859-1"] autorelease]];
    }
  }
}

- (void)load__WithJavaIoReader:(JavaIoReader *)inArg {
  @synchronized(self) {
    {
      if (inArg == nil) {
        @throw [[[JavaLangNullPointerException alloc] initWithNSString:@"in == null"] autorelease];
      }
      int mode = JavaUtilProperties_NONE, unicode = 0, count = 0;
      unichar nextChar;
      IOSCharArray *buf = [IOSCharArray arrayWithLength:40];
      int offset = 0, keyLength = -1, intVal;
      BOOL firstChar = YES;
      JavaIoBufferedReader *br = [[[JavaIoBufferedReader alloc] initWithJavaIoReader:inArg] autorelease];
      while (YES) {
        intVal = [((JavaIoBufferedReader *) nil_chk(br)) read];
        if (intVal == -1) {
          break;
        }
        nextChar = (unichar) intVal;
        if (offset == (int) [((IOSCharArray *) nil_chk(buf)) count]) {
          IOSCharArray *newBuf = [IOSCharArray arrayWithLength:(int) [((IOSCharArray *) nil_chk(buf)) count] * 2];
          [JavaLangSystem arraycopyWithId:buf withInt:0 withId:newBuf withInt:0 withInt:offset];
          buf = newBuf;
        }
        if (mode == JavaUtilProperties_UNICODE) {
          int digit = [JavaLangCharacter digitWithUnichar:nextChar withInt:16];
          if (digit >= 0) {
            unicode = (unicode << 4) + digit;
            if (++count < 4) {
              continue;
            }
          }
          else if (count <= 4) {
            @throw [[[JavaLangIllegalArgumentException alloc] initWithNSString:@"Invalid Unicode sequence: illegal character"] autorelease];
          }
          mode = JavaUtilProperties_NONE;
          (*[((IOSCharArray *) nil_chk(buf)) charRefAtIndex:offset++]) = (unichar) unicode;
          if (nextChar != 0x000a) {
            continue;
          }
        }
        if (mode == JavaUtilProperties_SLASH) {
          mode = JavaUtilProperties_NONE;
          switch (nextChar) {
            case 0x000d:
            mode = JavaUtilProperties_CONTINUE;
            continue;
            case 0x000a:
            mode = JavaUtilProperties_IGNORE;
            continue;
            case 'b':
            nextChar = 0x0008;
            break;
            case 'f':
            nextChar = 0x000c;
            break;
            case 'n':
            nextChar = 0x000a;
            break;
            case 'r':
            nextChar = 0x000d;
            break;
            case 't':
            nextChar = 0x0009;
            break;
            case 'u':
            mode = JavaUtilProperties_UNICODE;
            unicode = count = 0;
            continue;
          }
        }
        else {
          switch (nextChar) {
            case '#':
            case '!':
            if (firstChar) {
              while (YES) {
                intVal = [((JavaIoBufferedReader *) nil_chk(br)) read];
                if (intVal == -1) {
                  break;
                }
                nextChar = (unichar) intVal;
                if (nextChar == 0x000d || nextChar == 0x000a) {
                  break;
                }
              }
              continue;
            }
            break;
            case 0x000a:
            if (mode == JavaUtilProperties_CONTINUE) {
              mode = JavaUtilProperties_IGNORE;
              continue;
            }
            case 0x000d:
            mode = JavaUtilProperties_NONE;
            firstChar = YES;
            if (offset > 0 || (offset == 0 && keyLength == 0)) {
              if (keyLength == -1) {
                keyLength = offset;
              }
              NSString *temp = [NSString stringWithCharacters:buf offset:0 length:offset];
              [self putWithId:[((NSString *) nil_chk(temp)) substring:0 endIndex:keyLength] withId:[((NSString *) nil_chk(temp)) substring:keyLength]];
            }
            keyLength = -1;
            offset = 0;
            continue;
            case '\\':
            if (mode == JavaUtilProperties_KEY_DONE) {
              keyLength = offset;
            }
            mode = JavaUtilProperties_SLASH;
            continue;
            case ':':
            case '=':
            if (keyLength == -1) {
              mode = JavaUtilProperties_NONE;
              keyLength = offset;
              continue;
            }
            break;
          }
          if ([JavaLangCharacter isWhitespaceWithUnichar:nextChar]) {
            if (mode == JavaUtilProperties_CONTINUE) {
              mode = JavaUtilProperties_IGNORE;
            }
            if (offset == 0 || offset == keyLength || mode == JavaUtilProperties_IGNORE) {
              continue;
            }
            if (keyLength == -1) {
              mode = JavaUtilProperties_KEY_DONE;
              continue;
            }
          }
          if (mode == JavaUtilProperties_IGNORE || mode == JavaUtilProperties_CONTINUE) {
            mode = JavaUtilProperties_NONE;
          }
        }
        firstChar = NO;
        if (mode == JavaUtilProperties_KEY_DONE) {
          keyLength = offset;
          mode = JavaUtilProperties_NONE;
        }
        (*[((IOSCharArray *) nil_chk(buf)) charRefAtIndex:offset++]) = nextChar;
      }
      if (mode == JavaUtilProperties_UNICODE && count <= 4) {
        @throw [[[JavaLangIllegalArgumentException alloc] initWithNSString:@"Invalid Unicode sequence: expected format \\uxxxx"] autorelease];
      }
      if (keyLength == -1 && offset > 0) {
        keyLength = offset;
      }
      if (keyLength >= 0) {
        NSString *temp = [NSString stringWithCharacters:buf offset:0 length:offset];
        NSString *key = [((NSString *) nil_chk(temp)) substring:0 endIndex:keyLength];
        NSString *value = [((NSString *) nil_chk(temp)) substring:keyLength];
        if (mode == JavaUtilProperties_SLASH) {
          value = [NSString stringWithFormat:@"%@\x00", value];
        }
        [self putWithId:key withId:value];
      }
    }
  }
}

- (id<JavaUtilEnumeration>)propertyNames {
  JavaUtilHashtable *selected = [[[JavaUtilHashtable alloc] init] autorelease];
  [self selectPropertiesWithJavaUtilHashtable:selected withBOOL:NO];
  return [((JavaUtilHashtable *) nil_chk(selected)) keys];
}

- (id<JavaUtilSet>)stringPropertyNames {
  JavaUtilHashtable *stringProperties = [[[JavaUtilHashtable alloc] init] autorelease];
  [self selectPropertiesWithJavaUtilHashtable:stringProperties withBOOL:YES];
  return [JavaUtilCollections unmodifiableSetWithJavaUtilSet:[((JavaUtilHashtable *) nil_chk(stringProperties)) keySet]];
}

- (void)selectPropertiesWithJavaUtilHashtable:(JavaUtilHashtable *)selectProperties
                                     withBOOL:(BOOL)isStringOnly {
  if (defaults_ != nil) {
    [defaults_ selectPropertiesWithJavaUtilHashtable:selectProperties withBOOL:isStringOnly];
  }
  id<JavaUtilEnumeration> keys = [self keys];
  while ([((id<JavaUtilEnumeration>) nil_chk(keys)) hasMoreElements]) {
    id key = (id) [((id<JavaUtilEnumeration>) nil_chk(keys)) nextElement];
    if (isStringOnly && !([key isKindOfClass:[NSString class]])) {
      continue;
    }
    id value = [self getWithId:key];
    [((JavaUtilHashtable *) nil_chk(selectProperties)) putWithId:key withId:value];
  }
}

- (void)saveWithJavaIoOutputStream:(JavaIoOutputStream *)outArg
                      withNSString:(NSString *)comment {
  @try {
    [self storeWithJavaIoOutputStream:outArg withNSString:comment];
  }
  @catch (JavaIoIOException *e) {
  }
}

- (id)setPropertyWithNSString:(NSString *)name
                 withNSString:(NSString *)value {
  return [self putWithId:name withId:value];
}

- (void)storeWithJavaIoOutputStream:(JavaIoOutputStream *)outArg
                       withNSString:(NSString *)comment {
  @synchronized(self) {
    {
      [self storeWithJavaIoWriter:[[[JavaIoOutputStreamWriter alloc] initWithJavaIoOutputStream:outArg withNSString:@"ISO-8859-1"] autorelease] withNSString:comment];
    }
  }
}

- (void)storeWithJavaIoWriter:(JavaIoWriter *)writer
                 withNSString:(NSString *)comment {
  @synchronized(self) {
    {
      if (comment != nil) {
        [((JavaIoWriter *) nil_chk(writer)) writeWithNSString:@"#"];
        [((JavaIoWriter *) nil_chk(writer)) writeWithNSString:comment];
        [((JavaIoWriter *) nil_chk(writer)) writeWithNSString:[JavaLangSystem lineSeparator]];
      }
      [((JavaIoWriter *) nil_chk(writer)) writeWithNSString:@"#"];
      [((JavaIoWriter *) nil_chk(writer)) writeWithNSString:[((JavaUtilDate *) [[[JavaUtilDate alloc] init] autorelease]) description]];
      [((JavaIoWriter *) nil_chk(writer)) writeWithNSString:[JavaLangSystem lineSeparator]];
      JavaLangStringBuilder *sb = [[[JavaLangStringBuilder alloc] initWithInt:200] autorelease];
      {
        id<JavaUtilIterator> iter__ = [((id<JavaUtilSet>) nil_chk([self entrySet])) iterator];
        while ([((id<JavaUtilIterator>) nil_chk(iter__)) hasNext]) {
          id<JavaUtilMap_Entry> entry = [((id<JavaUtilIterator>) nil_chk(iter__)) next];
          NSString *key = (NSString *) [((id<JavaUtilMap_Entry>) nil_chk(entry)) getKey];
          [self dumpStringWithJavaLangStringBuilder:sb withNSString:key withBOOL:YES];
          [((JavaLangStringBuilder *) nil_chk(sb)) appendWithUnichar:'='];
          [self dumpStringWithJavaLangStringBuilder:sb withNSString:(NSString *) [((id<JavaUtilMap_Entry>) nil_chk(entry)) getValue] withBOOL:NO];
          [((JavaLangStringBuilder *) nil_chk(sb)) appendWithNSString:[JavaLangSystem lineSeparator]];
          [((JavaIoWriter *) nil_chk(writer)) writeWithNSString:[((JavaLangStringBuilder *) nil_chk(sb)) description]];
          [((JavaLangStringBuilder *) nil_chk(sb)) setLengthWithInt:0];
        }
      }
      [((JavaIoWriter *) nil_chk(writer)) flush];
    }
  }
}

- (void)loadFromXMLWithJavaIoInputStream:(JavaIoInputStream *)inArg {
  @synchronized(self) {
    {
      if (inArg == nil) {
        @throw [[[JavaLangNullPointerException alloc] initWithNSString:@"in == null"] autorelease];
      }
      @try {
        id<OrgXmlSaxXMLReader> reader = [OrgXmlSaxHelpersXMLReaderFactory createXMLReader];
        [((id<OrgXmlSaxXMLReader>) nil_chk(reader)) setContentHandlerWithOrgXmlSaxContentHandler:[[[JavaUtilProperties_$1 alloc] initWithJavaUtilProperties:self] autorelease]];
        [((id<OrgXmlSaxXMLReader>) nil_chk(reader)) parseWithOrgXmlSaxInputSource:[[[OrgXmlSaxInputSource alloc] initWithJavaIoInputStream:inArg] autorelease]];
      }
      @catch (OrgXmlSaxSAXException *e) {
        @throw [[[JavaUtilInvalidPropertiesFormatException alloc] initWithJavaLangThrowable:e] autorelease];
      }
    }
  }
}

- (void)storeToXMLWithJavaIoOutputStream:(JavaIoOutputStream *)os
                            withNSString:(NSString *)comment {
  [self storeToXMLWithJavaIoOutputStream:os withNSString:comment withNSString:@"UTF-8"];
}

- (void)storeToXMLWithJavaIoOutputStream:(JavaIoOutputStream *)os
                            withNSString:(NSString *)comment
                            withNSString:(NSString *)encoding {
  @synchronized(self) {
    {
      if (os == nil) {
        @throw [[[JavaLangNullPointerException alloc] initWithNSString:@"os == null"] autorelease];
      }
      else if (encoding == nil) {
        @throw [[[JavaLangNullPointerException alloc] initWithNSString:@"encoding == null"] autorelease];
      }
      NSString *encodingCanonicalName = @"UTF-8";
      JavaIoPrintStream *printStream = [[[JavaIoPrintStream alloc] initWithJavaIoOutputStream:os withBOOL:NO withNSString:encodingCanonicalName] autorelease];
      [((JavaIoPrintStream *) nil_chk(printStream)) printWithNSString:@"<?xml version=\"1.0\" encoding=\""];
      [((JavaIoPrintStream *) nil_chk(printStream)) printWithNSString:encodingCanonicalName];
      [((JavaIoPrintStream *) nil_chk(printStream)) printlnWithNSString:@"\"?>"];
      [((JavaIoPrintStream *) nil_chk(printStream)) printWithNSString:@"<!DOCTYPE properties SYSTEM \""];
      [((JavaIoPrintStream *) nil_chk(printStream)) printWithNSString:JavaUtilProperties_PROP_DTD_NAME_];
      [((JavaIoPrintStream *) nil_chk(printStream)) printlnWithNSString:@"\">"];
      [((JavaIoPrintStream *) nil_chk(printStream)) printlnWithNSString:@"<properties>"];
      if (comment != nil) {
        [((JavaIoPrintStream *) nil_chk(printStream)) printWithNSString:@"<comment>"];
        [((JavaIoPrintStream *) nil_chk(printStream)) printWithNSString:[self substitutePredefinedEntriesWithNSString:comment]];
        [((JavaIoPrintStream *) nil_chk(printStream)) printlnWithNSString:@"</comment>"];
      }
      {
        id<JavaUtilIterator> iter__ = [((id<JavaUtilSet>) nil_chk([self entrySet])) iterator];
        while ([((id<JavaUtilIterator>) nil_chk(iter__)) hasNext]) {
          id<JavaUtilMap_Entry> entry = [((id<JavaUtilIterator>) nil_chk(iter__)) next];
          NSString *keyValue = (NSString *) [((id<JavaUtilMap_Entry>) nil_chk(entry)) getKey];
          NSString *entryValue = (NSString *) [((id<JavaUtilMap_Entry>) nil_chk(entry)) getValue];
          [((JavaIoPrintStream *) nil_chk(printStream)) printWithNSString:@"<entry key=\""];
          [((JavaIoPrintStream *) nil_chk(printStream)) printWithNSString:[self substitutePredefinedEntriesWithNSString:keyValue]];
          [((JavaIoPrintStream *) nil_chk(printStream)) printWithNSString:@"\">"];
          [((JavaIoPrintStream *) nil_chk(printStream)) printWithNSString:[self substitutePredefinedEntriesWithNSString:entryValue]];
          [((JavaIoPrintStream *) nil_chk(printStream)) printlnWithNSString:@"</entry>"];
        }
      }
      [((JavaIoPrintStream *) nil_chk(printStream)) printlnWithNSString:@"</properties>"];
      [((JavaIoPrintStream *) nil_chk(printStream)) flush];
    }
  }
}

- (NSString *)substitutePredefinedEntriesWithNSString:(NSString *)s {
  s = [((NSString *) nil_chk(s)) replaceAll:@"&" withReplacement:@"&amp;"];
  s = [((NSString *) nil_chk(s)) replaceAll:@"<" withReplacement:@"&lt;"];
  s = [((NSString *) nil_chk(s)) replaceAll:@">" withReplacement:@"&gt;"];
  s = [((NSString *) nil_chk(s)) replaceAll:@"'" withReplacement:@"&apos;"];
  s = [((NSString *) nil_chk(s)) replaceAll:@"\"" withReplacement:@"&quot;"];
  return s;
}

- (void)dealloc {
  JreMemDebugRemove(self);
  JreOperatorRetainedAssign(&defaults_, self, nil);
  [super dealloc];
}

- (void)copyAllPropertiesTo:(id)copy {
  [super copyAllPropertiesTo:copy];
  JavaUtilProperties *typedCopy = (JavaUtilProperties *) copy;
  typedCopy.defaults = defaults_;
}

- (NSArray *)memDebugStrongReferences {
  NSMutableArray *result =
      [[[super memDebugStrongReferences] mutableCopy] autorelease];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:defaults_ name:@"defaults"]];
  return result;
}

@end
@implementation JavaUtilProperties_$1

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  return result;
}

- (JavaUtilProperties *)this$0 {
  return this$0_;
}
- (void)setThis$0:(JavaUtilProperties *)this$0 {
  JreOperatorRetainedAssign(&this$0_, self, this$0);
}
@synthesize this$0 = this$0_;
- (NSString *)key {
  return key_;
}
- (void)setKey:(NSString *)key {
  JreOperatorRetainedAssign(&key_, self, key);
}
@synthesize key = key_;

- (void)startElementWithNSString:(NSString *)uri
                    withNSString:(NSString *)localName
                    withNSString:(NSString *)qName
         withOrgXmlSaxAttributes:(id<OrgXmlSaxAttributes>)attributes {
  JreOperatorRetainedAssign(&key_, self, nil);
  if ([((NSString *) nil_chk(qName)) isEqual:@"entry"]) {
    JreOperatorRetainedAssign(&key_, self, [((id<OrgXmlSaxAttributes>) nil_chk(attributes)) getValueWithNSString:@"key"]);
  }
}

- (void)charactersWithCharArray:(IOSCharArray *)ch
                        withInt:(int)start
                        withInt:(int)length {
  if (key_ != nil) {
    NSString *value = [NSString stringWithCharacters:ch offset:start length:length];
    [this$0_ putWithId:key_ withId:value];
    JreOperatorRetainedAssign(&key_, self, nil);
  }
}

- (id)initWithJavaUtilProperties:(JavaUtilProperties *)outer$ {
  if ((self = [super init])) {
    JreOperatorRetainedAssign(&this$0_, self, outer$);
    JreMemDebugAdd(self);
  }
  return self;
}

- (void)dealloc {
  JreMemDebugRemove(self);
  JreOperatorRetainedAssign(&key_, self, nil);
  JreOperatorRetainedAssign(&this$0_, self, nil);
  [super dealloc];
}

- (void)copyAllPropertiesTo:(id)copy {
  [super copyAllPropertiesTo:copy];
  JavaUtilProperties_$1 *typedCopy = (JavaUtilProperties_$1 *) copy;
  typedCopy.this$0 = this$0_;
  typedCopy.key = key_;
}

- (NSArray *)memDebugStrongReferences {
  NSMutableArray *result =
      [[[super memDebugStrongReferences] mutableCopy] autorelease];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:this$0_ name:@"this$0"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:key_ name:@"key"]];
  return result;
}

@end
