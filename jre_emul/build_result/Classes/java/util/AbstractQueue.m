//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: apache_harmony/classlib/modules/luni/src/main/java/java/util/AbstractQueue.java
//
//  Created by stha1de on 09.08.13.
//

#include "java/lang/IllegalArgumentException.h"
#include "java/lang/IllegalStateException.h"
#include "java/lang/NullPointerException.h"
#include "java/util/AbstractQueue.h"
#include "java/util/Collection.h"
#include "java/util/NoSuchElementException.h"
#include "java/util/Queue.h"

@implementation JavaUtilAbstractQueue

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  return result;
}

- (id)init {
  return JreMemDebugAdd([super init]);
}

- (BOOL)addWithId:(id)o {
  if (nil == o) {
    @throw [[[JavaLangNullPointerException alloc] init] autorelease];
  }
  if ([self offerWithId:o]) {
    return YES;
  }
  @throw [[[JavaLangIllegalStateException alloc] init] autorelease];
}

- (BOOL)addAllWithJavaUtilCollection:(id<JavaUtilCollection>)c {
  if (nil == c) {
    @throw [[[JavaLangNullPointerException alloc] init] autorelease];
  }
  if (self == c) {
    @throw [[[JavaLangIllegalArgumentException alloc] init] autorelease];
  }
  return [super addAllWithJavaUtilCollection:c];
}

- (id)remove {
  id o = [self poll];
  if (nil == o) {
    @throw [[[JavaUtilNoSuchElementException alloc] init] autorelease];
  }
  return o;
}

- (id)element {
  id o = [self peek];
  if (nil == o) {
    @throw [[[JavaUtilNoSuchElementException alloc] init] autorelease];
  }
  return o;
}

- (void)clear {
  id o;
  do {
    o = [self poll];
  }
  while (nil != o);
}

- (BOOL)offerWithId:(id)param0 {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (id)peek {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (id)poll {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (void)dealloc {
  JreMemDebugRemove(self);
  [super dealloc];
}

@end
