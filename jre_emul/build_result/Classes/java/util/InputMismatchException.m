//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: apache_harmony/classlib/modules/luni/src/main/java/java/util/InputMismatchException.java
//
//  Created by stha1de on 09.08.13.
//

#include "java/util/InputMismatchException.h"

@implementation JavaUtilInputMismatchException

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  return result;
}

- (id)init {
  return JreMemDebugAdd([super init]);
}

- (id)initWithNSString:(NSString *)msg {
  return JreMemDebugAdd([super initWithNSString:msg]);
}

- (void)dealloc {
  JreMemDebugRemove(self);
  [super dealloc];
}

@end
