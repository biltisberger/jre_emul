//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: android/libcore/luni/src/main/java/java/security/Permission.java
//
//  Created by stha1de on 09.08.13.
//

#include "java/security/AllPermissionCollection.h"
#include "java/security/Permission.h"
#include "java/security/PermissionCollection.h"

@implementation JavaSecurityPermission

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  return result;
}

- (id)initWithNSString:(NSString *)name {
  return JreMemDebugAdd([super init]);
}

- (NSString *)getName {
  return nil;
}

- (void)checkGuardWithId:(id)obj {
}

- (JavaSecurityPermissionCollection *)newPermissionCollection OBJC_METHOD_FAMILY_NONE {
  return [[[JavaSecurityAllPermissionCollection alloc] init] autorelease];
}

- (NSString *)getActions {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (BOOL)impliesWithJavaSecurityPermission:(JavaSecurityPermission *)permission {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (void)dealloc {
  JreMemDebugRemove(self);
  [super dealloc];
}

@end
