//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: apache_harmony/classlib/modules/luni/src/main/java/java/io/StringWriter.java
//
//  Created by stha1de on 09.08.13.
//

#include "IOSCharArray.h"
#include "java/io/StringWriter.h"
#include "java/io/Writer.h"
#include "java/lang/CharSequence.h"
#include "java/lang/IllegalArgumentException.h"
#include "java/lang/IndexOutOfBoundsException.h"
#include "java/lang/StringBuffer.h"

@implementation JavaIoStringWriter

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  return result;
}

- (JavaLangStringBuffer *)buf {
  return buf_;
}
- (void)setBuf:(JavaLangStringBuffer *)buf {
  JreOperatorRetainedAssign(&buf_, self, buf);
}
@synthesize buf = buf_;

- (id)init {
  if ((self = [super init])) {
    JreOperatorRetainedAssign(&buf_, self, [[[JavaLangStringBuffer alloc] initWithInt:16] autorelease]);
    JreOperatorRetainedAssign(&lock_, self, buf_);
    JreMemDebugAdd(self);
  }
  return self;
}

- (id)initWithInt:(int)initialSize {
  if ((self = [super init])) {
    if (initialSize < 0) {
      @throw [[[JavaLangIllegalArgumentException alloc] init] autorelease];
    }
    JreOperatorRetainedAssign(&buf_, self, [[[JavaLangStringBuffer alloc] initWithInt:initialSize] autorelease]);
    JreOperatorRetainedAssign(&lock_, self, buf_);
    JreMemDebugAdd(self);
  }
  return self;
}

- (void)close {
}

- (void)flush {
}

- (JavaLangStringBuffer *)getBuffer {
  return buf_;
}

- (NSString *)description {
  return [((JavaLangStringBuffer *) nil_chk(buf_)) description];
}

- (void)writeWithCharArray:(IOSCharArray *)cbuf
                   withInt:(int)offset
                   withInt:(int)count {
  if (offset < 0 || offset > (int) [((IOSCharArray *) nil_chk(cbuf)) count] || count < 0 || count > (int) [((IOSCharArray *) nil_chk(cbuf)) count] - offset) {
    @throw [[[JavaLangIndexOutOfBoundsException alloc] init] autorelease];
  }
  if (count == 0) {
    return;
  }
  [((JavaLangStringBuffer *) nil_chk(buf_)) appendWithCharArray:cbuf withInt:offset withInt:count];
}

- (void)writeWithInt:(int)oneChar {
  [((JavaLangStringBuffer *) nil_chk(buf_)) appendWithUnichar:(unichar) oneChar];
}

- (void)writeWithNSString:(NSString *)str {
  [((JavaLangStringBuffer *) nil_chk(buf_)) appendWithNSString:str];
}

- (void)writeWithNSString:(NSString *)str
                  withInt:(int)offset
                  withInt:(int)count {
  NSString *sub = [((NSString *) nil_chk(str)) substring:offset endIndex:offset + count];
  [((JavaLangStringBuffer *) nil_chk(buf_)) appendWithNSString:sub];
}

- (JavaIoStringWriter *)appendWithUnichar:(unichar)c {
  [self writeWithInt:c];
  return self;
}

- (JavaIoStringWriter *)appendWithJavaLangCharSequence:(id<JavaLangCharSequence>)csq {
  if (nil == csq) {
    [self writeWithNSString:[JavaIoWriter TOKEN_NULL]];
  }
  else {
    [self writeWithNSString:[csq sequenceDescription]];
  }
  return self;
}

- (JavaIoStringWriter *)appendWithJavaLangCharSequence:(id<JavaLangCharSequence>)csq
                                               withInt:(int)start
                                               withInt:(int)end {
  if (nil == csq) {
    csq = (id<JavaLangCharSequence>) [JavaIoWriter TOKEN_NULL];
  }
  NSString *output = [((id<JavaLangCharSequence>) nil_chk([((id<JavaLangCharSequence>) nil_chk(csq)) subSequenceFrom:start to:end])) sequenceDescription];
  [self writeWithNSString:output withInt:0 withInt:[((NSString *) nil_chk(output)) length]];
  return self;
}

- (void)dealloc {
  JreMemDebugRemove(self);
  JreOperatorRetainedAssign(&buf_, self, nil);
  [super dealloc];
}

- (void)copyAllPropertiesTo:(id)copy {
  [super copyAllPropertiesTo:copy];
  JavaIoStringWriter *typedCopy = (JavaIoStringWriter *) copy;
  typedCopy.buf = buf_;
}

- (NSArray *)memDebugStrongReferences {
  NSMutableArray *result =
      [[[super memDebugStrongReferences] mutableCopy] autorelease];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:buf_ name:@"buf"]];
  return result;
}

@end
