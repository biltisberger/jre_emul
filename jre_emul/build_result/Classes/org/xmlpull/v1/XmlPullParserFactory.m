//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: android/libcore/xml/src/main/java/org/xmlpull/v1/XmlPullParserFactory.java
//
//  Created by stha1de on 09.08.13.
//

#include "IOSClass.h"
#include "java/lang/Boolean.h"
#include "java/lang/Exception.h"
#include "java/lang/StringBuilder.h"
#include "java/util/ArrayList.h"
#include "java/util/HashMap.h"
#include "java/util/Iterator.h"
#include "java/util/Set.h"
#include "org/xmlpull/v1/XmlPullParser.h"
#include "org/xmlpull/v1/XmlPullParserException.h"
#include "org/xmlpull/v1/XmlPullParserFactory.h"
#include "org/xmlpull/v1/XmlSerializer.h"

@implementation OrgXmlpullV1XmlPullParserFactory

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlpullV1XmlPullParserFactory_referenceContextClass_ name:@"OrgXmlpullV1XmlPullParserFactory_referenceContextClass_"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlpullV1XmlPullParserFactory_PROPERTY_NAME_ name:@"OrgXmlpullV1XmlPullParserFactory_PROPERTY_NAME_"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlpullV1XmlPullParserFactory_RESOURCE_NAME_ name:@"OrgXmlpullV1XmlPullParserFactory_RESOURCE_NAME_"]];
  return result;
}

static IOSClass * OrgXmlpullV1XmlPullParserFactory_referenceContextClass_;
static NSString * OrgXmlpullV1XmlPullParserFactory_PROPERTY_NAME_ = @"org.xmlpull.v1.XmlPullParserFactory";
static NSString * OrgXmlpullV1XmlPullParserFactory_RESOURCE_NAME_ = @"/META-INF/services/org.xmlpull.v1.XmlPullParserFactory";

- (JavaUtilArrayList *)parserClasses {
  return parserClasses_;
}
- (void)setParserClasses:(JavaUtilArrayList *)parserClasses {
  JreOperatorRetainedAssign(&parserClasses_, self, parserClasses);
}
@synthesize parserClasses = parserClasses_;
- (NSString *)classNamesLocation {
  return classNamesLocation_;
}
- (void)setClassNamesLocation:(NSString *)classNamesLocation {
  JreOperatorRetainedAssign(&classNamesLocation_, self, classNamesLocation);
}
@synthesize classNamesLocation = classNamesLocation_;
- (JavaUtilArrayList *)serializerClasses {
  return serializerClasses_;
}
- (void)setSerializerClasses:(JavaUtilArrayList *)serializerClasses {
  JreOperatorRetainedAssign(&serializerClasses_, self, serializerClasses);
}
@synthesize serializerClasses = serializerClasses_;
- (JavaUtilHashMap *)features {
  return features_;
}
- (void)setFeatures:(JavaUtilHashMap *)features {
  JreOperatorRetainedAssign(&features_, self, features);
}
@synthesize features = features_;

+ (IOSClass *)referenceContextClass {
  return OrgXmlpullV1XmlPullParserFactory_referenceContextClass_;
}

+ (NSString *)PROPERTY_NAME {
  return OrgXmlpullV1XmlPullParserFactory_PROPERTY_NAME_;
}

+ (NSString *)RESOURCE_NAME {
  return OrgXmlpullV1XmlPullParserFactory_RESOURCE_NAME_;
}

- (id)init {
  if ((self = [super init])) {
    JreOperatorRetainedAssign(&features_, self, [[[JavaUtilHashMap alloc] init] autorelease]);
    JreMemDebugAdd(self);
  }
  return self;
}

- (void)setFeatureWithNSString:(NSString *)name
                      withBOOL:(BOOL)state {
  [((JavaUtilHashMap *) nil_chk(features_)) putWithId:name withId:[JavaLangBoolean valueOfWithBOOL:state]];
}

- (BOOL)getFeatureWithNSString:(NSString *)name {
  JavaLangBoolean *value = (JavaLangBoolean *) [((JavaUtilHashMap *) nil_chk(features_)) getWithId:name];
  return value != nil ? [((JavaLangBoolean *) nil_chk(value)) booleanValue] : NO;
}

- (void)setNamespaceAwareWithBOOL:(BOOL)awareness {
  [((JavaUtilHashMap *) nil_chk(features_)) putWithId:[OrgXmlpullV1XmlPullParser FEATURE_PROCESS_NAMESPACES] withId:[JavaLangBoolean valueOfWithBOOL:awareness]];
}

- (BOOL)isNamespaceAware {
  return [self getFeatureWithNSString:[OrgXmlpullV1XmlPullParser FEATURE_PROCESS_NAMESPACES]];
}

- (void)setValidatingWithBOOL:(BOOL)validating {
  [((JavaUtilHashMap *) nil_chk(features_)) putWithId:[OrgXmlpullV1XmlPullParser FEATURE_VALIDATION] withId:[JavaLangBoolean valueOfWithBOOL:validating]];
}

- (BOOL)isValidating {
  return [self getFeatureWithNSString:[OrgXmlpullV1XmlPullParser FEATURE_VALIDATION]];
}

- (id<OrgXmlpullV1XmlPullParser>)newPullParser OBJC_METHOD_FAMILY_NONE {
  if (parserClasses_ == nil) @throw [[[OrgXmlpullV1XmlPullParserException alloc] initWithNSString:[NSString stringWithFormat:@"Factory initialization was incomplete - has not tried %@", classNamesLocation_]] autorelease];
  if ([((JavaUtilArrayList *) nil_chk(parserClasses_)) size] == 0) @throw [[[OrgXmlpullV1XmlPullParserException alloc] initWithNSString:[NSString stringWithFormat:@"No valid parser classes found in %@", classNamesLocation_]] autorelease];
  JavaLangStringBuilder *issues = [[[JavaLangStringBuilder alloc] init] autorelease];
  for (int i = 0; i < [((JavaUtilArrayList *) nil_chk(parserClasses_)) size]; i++) {
    IOSClass *ppClass = (IOSClass *) [((JavaUtilArrayList *) nil_chk(parserClasses_)) getWithInt:i];
    @try {
      id<OrgXmlpullV1XmlPullParser> pp = (id<OrgXmlpullV1XmlPullParser>) [((IOSClass *) nil_chk(ppClass)) newInstance];
      for (id<JavaUtilIterator> iter = [((id<JavaUtilSet>) nil_chk([((JavaUtilHashMap *) nil_chk(features_)) keySet])) iterator]; [((id<JavaUtilIterator>) nil_chk(iter)) hasNext]; ) {
        NSString *key = (NSString *) [((id<JavaUtilIterator>) nil_chk(iter)) next];
        JavaLangBoolean *value = (JavaLangBoolean *) [((JavaUtilHashMap *) nil_chk(features_)) getWithId:key];
        if (value != nil && [((JavaLangBoolean *) nil_chk(value)) booleanValue]) {
          [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) setFeatureWithNSString:key withBOOL:YES];
        }
      }
      return pp;
    }
    @catch (JavaLangException *ex) {
      [((JavaLangStringBuilder *) nil_chk(issues)) appendWithNSString:[NSString stringWithFormat:@"%@: %@; ", [((IOSClass *) nil_chk(ppClass)) getName], [((JavaLangException *) nil_chk(ex)) description]]];
    }
  }
  @throw [[[OrgXmlpullV1XmlPullParserException alloc] initWithNSString:[NSString stringWithFormat:@"could not create parser: %@", issues]] autorelease];
}

- (id<OrgXmlpullV1XmlSerializer>)newSerializer OBJC_METHOD_FAMILY_NONE {
  if (serializerClasses_ == nil) {
    @throw [[[OrgXmlpullV1XmlPullParserException alloc] initWithNSString:[NSString stringWithFormat:@"Factory initialization incomplete - has not tried %@", classNamesLocation_]] autorelease];
  }
  if ([((JavaUtilArrayList *) nil_chk(serializerClasses_)) size] == 0) {
    @throw [[[OrgXmlpullV1XmlPullParserException alloc] initWithNSString:[NSString stringWithFormat:@"No valid serializer classes found in %@", classNamesLocation_]] autorelease];
  }
  JavaLangStringBuilder *issues = [[[JavaLangStringBuilder alloc] init] autorelease];
  for (int i = 0; i < [((JavaUtilArrayList *) nil_chk(serializerClasses_)) size]; i++) {
    IOSClass *ppClass = (IOSClass *) [((JavaUtilArrayList *) nil_chk(serializerClasses_)) getWithInt:i];
    @try {
      id<OrgXmlpullV1XmlSerializer> ser = (id<OrgXmlpullV1XmlSerializer>) [((IOSClass *) nil_chk(ppClass)) newInstance];
      return ser;
    }
    @catch (JavaLangException *ex) {
      [((JavaLangStringBuilder *) nil_chk(issues)) appendWithNSString:[NSString stringWithFormat:@"%@: %@; ", [((IOSClass *) nil_chk(ppClass)) getName], [((JavaLangException *) nil_chk(ex)) description]]];
    }
  }
  @throw [[[OrgXmlpullV1XmlPullParserException alloc] initWithNSString:[NSString stringWithFormat:@"could not create serializer: %@", issues]] autorelease];
}

+ (OrgXmlpullV1XmlPullParserFactory *)newInstance OBJC_METHOD_FAMILY_NONE {
  return [OrgXmlpullV1XmlPullParserFactory newInstanceWithNSString:nil withIOSClass:nil];
}

+ (OrgXmlpullV1XmlPullParserFactory *)newInstanceWithNSString:(NSString *)classNames
                                                 withIOSClass:(IOSClass *)context OBJC_METHOD_FAMILY_NONE {
  classNames = @"org.kxml2.io.KXmlParser,org.kxml2.io.KXmlSerializer";
  OrgXmlpullV1XmlPullParserFactory *factory = nil;
  JavaUtilArrayList *parserClasses = [[[JavaUtilArrayList alloc] init] autorelease];
  JavaUtilArrayList *serializerClasses = [[[JavaUtilArrayList alloc] init] autorelease];
  int pos = 0;
  while (pos < [((NSString *) nil_chk(classNames)) length]) {
    int cut = [((NSString *) nil_chk(classNames)) indexOf:',' fromIndex:pos];
    if (cut == -1) cut = [((NSString *) nil_chk(classNames)) length];
    NSString *name = [((NSString *) nil_chk(classNames)) substring:pos endIndex:cut];
    IOSClass *candidate = nil;
    id instance = nil;
    @try {
      candidate = [IOSClass forName:name];
      instance = [((IOSClass *) nil_chk(candidate)) newInstance];
    }
    @catch (JavaLangException *e) {
    }
    if (candidate != nil) {
      BOOL recognized = NO;
      if ([instance conformsToProtocol: @protocol(OrgXmlpullV1XmlPullParser)]) {
        [((JavaUtilArrayList *) nil_chk(parserClasses)) addWithId:candidate];
        recognized = YES;
      }
      if ([instance conformsToProtocol: @protocol(OrgXmlpullV1XmlSerializer)]) {
        [((JavaUtilArrayList *) nil_chk(serializerClasses)) addWithId:candidate];
        recognized = YES;
      }
      if ([instance isKindOfClass:[OrgXmlpullV1XmlPullParserFactory class]]) {
        if (factory == nil) {
          factory = (OrgXmlpullV1XmlPullParserFactory *) instance;
        }
        recognized = YES;
      }
      if (!recognized) {
        @throw [[[OrgXmlpullV1XmlPullParserException alloc] initWithNSString:[NSString stringWithFormat:@"incompatible class: %@", name]] autorelease];
      }
    }
    pos = cut + 1;
  }
  if (factory == nil) {
    factory = [[[OrgXmlpullV1XmlPullParserFactory alloc] init] autorelease];
  }
  ((OrgXmlpullV1XmlPullParserFactory *) nil_chk(factory)).parserClasses = parserClasses;
  ((OrgXmlpullV1XmlPullParserFactory *) nil_chk(factory)).serializerClasses = serializerClasses;
  ((OrgXmlpullV1XmlPullParserFactory *) nil_chk(factory)).classNamesLocation = @"org.kxml2.io.kXmlParser,org.kxml2.io.KXmlSerializer";
  return factory;
}

+ (void)initialize {
  if (self == [OrgXmlpullV1XmlPullParserFactory class]) {
    {
      OrgXmlpullV1XmlPullParserFactory *f = [[[OrgXmlpullV1XmlPullParserFactory alloc] init] autorelease];
      JreOperatorRetainedAssign(&OrgXmlpullV1XmlPullParserFactory_referenceContextClass_, self, [((OrgXmlpullV1XmlPullParserFactory *) nil_chk(f)) getClass]);
    }
  }
}

- (void)dealloc {
  JreMemDebugRemove(self);
  JreOperatorRetainedAssign(&features_, self, nil);
  JreOperatorRetainedAssign(&serializerClasses_, self, nil);
  JreOperatorRetainedAssign(&classNamesLocation_, self, nil);
  JreOperatorRetainedAssign(&parserClasses_, self, nil);
  [super dealloc];
}

- (void)copyAllPropertiesTo:(id)copy {
  [super copyAllPropertiesTo:copy];
  OrgXmlpullV1XmlPullParserFactory *typedCopy = (OrgXmlpullV1XmlPullParserFactory *) copy;
  typedCopy.parserClasses = parserClasses_;
  typedCopy.classNamesLocation = classNamesLocation_;
  typedCopy.serializerClasses = serializerClasses_;
  typedCopy.features = features_;
}

- (NSArray *)memDebugStrongReferences {
  NSMutableArray *result =
      [[[super memDebugStrongReferences] mutableCopy] autorelease];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:parserClasses_ name:@"parserClasses"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:classNamesLocation_ name:@"classNamesLocation"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:serializerClasses_ name:@"serializerClasses"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:features_ name:@"features"]];
  return result;
}

@end
