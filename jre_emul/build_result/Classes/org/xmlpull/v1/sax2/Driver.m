//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: android/libcore/xml/src/main/java/org/xmlpull/v1/sax2/Driver.java
//
//  Created by stha1de on 09.08.13.
//

#include "IOSCharArray.h"
#include "IOSClass.h"
#include "IOSIntArray.h"
#include "java/io/FileInputStream.h"
#include "java/io/FileNotFoundException.h"
#include "java/io/InputStream.h"
#include "java/io/Reader.h"
#include "java/lang/StringBuilder.h"
#include "org/xml/sax/ContentHandler.h"
#include "org/xml/sax/DTDHandler.h"
#include "org/xml/sax/EntityResolver.h"
#include "org/xml/sax/ErrorHandler.h"
#include "org/xml/sax/InputSource.h"
#include "org/xml/sax/SAXException.h"
#include "org/xml/sax/SAXNotSupportedException.h"
#include "org/xml/sax/SAXParseException.h"
#include "org/xml/sax/helpers/DefaultHandler.h"
#include "org/xmlpull/v1/XmlPullParser.h"
#include "org/xmlpull/v1/XmlPullParserException.h"
#include "org/xmlpull/v1/XmlPullParserFactory.h"
#include "org/xmlpull/v1/sax2/Driver.h"

@implementation OrgXmlpullV1Sax2Driver

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlpullV1Sax2Driver_DECLARATION_HANDLER_PROPERTY_ name:@"OrgXmlpullV1Sax2Driver_DECLARATION_HANDLER_PROPERTY_"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlpullV1Sax2Driver_LEXICAL_HANDLER_PROPERTY_ name:@"OrgXmlpullV1Sax2Driver_LEXICAL_HANDLER_PROPERTY_"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlpullV1Sax2Driver_NAMESPACES_FEATURE_ name:@"OrgXmlpullV1Sax2Driver_NAMESPACES_FEATURE_"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlpullV1Sax2Driver_NAMESPACE_PREFIXES_FEATURE_ name:@"OrgXmlpullV1Sax2Driver_NAMESPACE_PREFIXES_FEATURE_"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlpullV1Sax2Driver_VALIDATION_FEATURE_ name:@"OrgXmlpullV1Sax2Driver_VALIDATION_FEATURE_"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlpullV1Sax2Driver_APACHE_SCHEMA_VALIDATION_FEATURE_ name:@"OrgXmlpullV1Sax2Driver_APACHE_SCHEMA_VALIDATION_FEATURE_"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlpullV1Sax2Driver_APACHE_DYNAMIC_VALIDATION_FEATURE_ name:@"OrgXmlpullV1Sax2Driver_APACHE_DYNAMIC_VALIDATION_FEATURE_"]];
  return result;
}

static NSString * OrgXmlpullV1Sax2Driver_DECLARATION_HANDLER_PROPERTY_ = @"http://xml.org/sax/properties/declaration-handler";
static NSString * OrgXmlpullV1Sax2Driver_LEXICAL_HANDLER_PROPERTY_ = @"http://xml.org/sax/properties/lexical-handler";
static NSString * OrgXmlpullV1Sax2Driver_NAMESPACES_FEATURE_ = @"http://xml.org/sax/features/namespaces";
static NSString * OrgXmlpullV1Sax2Driver_NAMESPACE_PREFIXES_FEATURE_ = @"http://xml.org/sax/features/namespace-prefixes";
static NSString * OrgXmlpullV1Sax2Driver_VALIDATION_FEATURE_ = @"http://xml.org/sax/features/validation";
static NSString * OrgXmlpullV1Sax2Driver_APACHE_SCHEMA_VALIDATION_FEATURE_ = @"http://apache.org/xml/features/validation/schema";
static NSString * OrgXmlpullV1Sax2Driver_APACHE_DYNAMIC_VALIDATION_FEATURE_ = @"http://apache.org/xml/features/validation/dynamic";

- (id<OrgXmlSaxContentHandler>)contentHandler {
  return contentHandler_;
}
- (void)setContentHandler:(id<OrgXmlSaxContentHandler>)contentHandler {
  JreOperatorRetainedAssign(&contentHandler_, self, contentHandler);
}
@synthesize contentHandler = contentHandler_;
- (id<OrgXmlSaxErrorHandler>)errorHandler {
  return errorHandler_;
}
- (void)setErrorHandler:(id<OrgXmlSaxErrorHandler>)errorHandler {
  JreOperatorRetainedAssign(&errorHandler_, self, errorHandler);
}
@synthesize errorHandler = errorHandler_;
- (NSString *)systemId {
  return systemId_;
}
- (void)setSystemId:(NSString *)systemId {
  JreOperatorRetainedAssign(&systemId_, self, systemId);
}
@synthesize systemId = systemId_;
- (id<OrgXmlpullV1XmlPullParser>)pp {
  return pp_;
}
- (void)setPp:(id<OrgXmlpullV1XmlPullParser>)pp {
  JreOperatorRetainedAssign(&pp_, self, pp);
}
@synthesize pp = pp_;

+ (NSString *)DECLARATION_HANDLER_PROPERTY {
  return OrgXmlpullV1Sax2Driver_DECLARATION_HANDLER_PROPERTY_;
}

+ (NSString *)LEXICAL_HANDLER_PROPERTY {
  return OrgXmlpullV1Sax2Driver_LEXICAL_HANDLER_PROPERTY_;
}

+ (NSString *)NAMESPACES_FEATURE {
  return OrgXmlpullV1Sax2Driver_NAMESPACES_FEATURE_;
}

+ (NSString *)NAMESPACE_PREFIXES_FEATURE {
  return OrgXmlpullV1Sax2Driver_NAMESPACE_PREFIXES_FEATURE_;
}

+ (NSString *)VALIDATION_FEATURE {
  return OrgXmlpullV1Sax2Driver_VALIDATION_FEATURE_;
}

+ (NSString *)APACHE_SCHEMA_VALIDATION_FEATURE {
  return OrgXmlpullV1Sax2Driver_APACHE_SCHEMA_VALIDATION_FEATURE_;
}

+ (NSString *)APACHE_DYNAMIC_VALIDATION_FEATURE {
  return OrgXmlpullV1Sax2Driver_APACHE_DYNAMIC_VALIDATION_FEATURE_;
}

- (id)init {
  if ((self = [super init])) {
    JreOperatorRetainedAssign(&contentHandler_, self, [[[OrgXmlSaxHelpersDefaultHandler alloc] init] autorelease]);
    JreOperatorRetainedAssign(&errorHandler_, self, [[[OrgXmlSaxHelpersDefaultHandler alloc] init] autorelease]);
    OrgXmlpullV1XmlPullParserFactory *factory = [OrgXmlpullV1XmlPullParserFactory newInstance];
    [((OrgXmlpullV1XmlPullParserFactory *) nil_chk(factory)) setNamespaceAwareWithBOOL:YES];
    JreOperatorRetainedAssign(&pp_, self, [((OrgXmlpullV1XmlPullParserFactory *) nil_chk(factory)) newPullParser]);
    JreMemDebugAdd(self);
  }
  return self;
}

- (id)initWithOrgXmlpullV1XmlPullParser:(id<OrgXmlpullV1XmlPullParser>)pp {
  if ((self = [super init])) {
    JreOperatorRetainedAssign(&contentHandler_, self, [[[OrgXmlSaxHelpersDefaultHandler alloc] init] autorelease]);
    JreOperatorRetainedAssign(&errorHandler_, self, [[[OrgXmlSaxHelpersDefaultHandler alloc] init] autorelease]);
    self.pp = pp;
    JreMemDebugAdd(self);
  }
  return self;
}

- (int)getLength {
  return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeCount];
}

- (NSString *)getURIWithInt:(int)index {
  return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeNamespaceWithInt:index];
}

- (NSString *)getLocalNameWithInt:(int)index {
  return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeNameWithInt:index];
}

- (NSString *)getQNameWithInt:(int)index {
  NSString *prefix = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributePrefixWithInt:index];
  if (prefix != nil) {
    return [NSString stringWithFormat:@"%@:%@", prefix, [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeNameWithInt:index]];
  }
  else {
    return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeNameWithInt:index];
  }
}

- (NSString *)getTypeWithInt:(int)index {
  return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeTypeWithInt:index];
}

- (NSString *)getValueWithInt:(int)index {
  return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeValueWithInt:index];
}

- (int)getIndexWithNSString:(NSString *)uri
               withNSString:(NSString *)localName {
  for (int i = 0; i < [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeCount]; i++) {
    if ([((NSString *) nil_chk([((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeNamespaceWithInt:i])) isEqual:uri] && [((NSString *) nil_chk([((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeNameWithInt:i])) isEqual:localName]) {
      return i;
    }
  }
  return -1;
}

- (int)getIndexWithNSString:(NSString *)qName {
  for (int i = 0; i < [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeCount]; i++) {
    if ([((NSString *) nil_chk([((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeNameWithInt:i])) isEqual:qName]) {
      return i;
    }
  }
  return -1;
}

- (NSString *)getTypeWithNSString:(NSString *)uri
                     withNSString:(NSString *)localName {
  for (int i = 0; i < [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeCount]; i++) {
    if ([((NSString *) nil_chk([((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeNamespaceWithInt:i])) isEqual:uri] && [((NSString *) nil_chk([((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeNameWithInt:i])) isEqual:localName]) {
      return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeTypeWithInt:i];
    }
  }
  return nil;
}

- (NSString *)getTypeWithNSString:(NSString *)qName {
  for (int i = 0; i < [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeCount]; i++) {
    if ([((NSString *) nil_chk([((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeNameWithInt:i])) isEqual:qName]) {
      return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeTypeWithInt:i];
    }
  }
  return nil;
}

- (NSString *)getValueWithNSString:(NSString *)uri
                      withNSString:(NSString *)localName {
  return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeValueWithNSString:uri withNSString:localName];
}

- (NSString *)getValueWithNSString:(NSString *)qName {
  return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getAttributeValueWithNSString:nil withNSString:qName];
}

- (NSString *)getPublicId {
  return nil;
}

- (NSString *)getSystemId {
  return systemId_;
}

- (int)getLineNumber {
  return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getLineNumber];
}

- (int)getColumnNumber {
  return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getColumnNumber];
}

- (BOOL)getFeatureWithNSString:(NSString *)name {
  if ([((NSString *) nil_chk(OrgXmlpullV1Sax2Driver_NAMESPACES_FEATURE_)) isEqual:name]) {
    return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getFeatureWithNSString:[OrgXmlpullV1XmlPullParser FEATURE_PROCESS_NAMESPACES]];
  }
  else if ([((NSString *) nil_chk(OrgXmlpullV1Sax2Driver_NAMESPACE_PREFIXES_FEATURE_)) isEqual:name]) {
    return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getFeatureWithNSString:[OrgXmlpullV1XmlPullParser FEATURE_REPORT_NAMESPACE_ATTRIBUTES]];
  }
  else if ([((NSString *) nil_chk(OrgXmlpullV1Sax2Driver_VALIDATION_FEATURE_)) isEqual:name]) {
    return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getFeatureWithNSString:[OrgXmlpullV1XmlPullParser FEATURE_VALIDATION]];
  }
  else {
    return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getFeatureWithNSString:name];
  }
}

- (void)setFeatureWithNSString:(NSString *)name
                      withBOOL:(BOOL)value {
  @try {
    if ([((NSString *) nil_chk(OrgXmlpullV1Sax2Driver_NAMESPACES_FEATURE_)) isEqual:name]) {
      [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) setFeatureWithNSString:[OrgXmlpullV1XmlPullParser FEATURE_PROCESS_NAMESPACES] withBOOL:value];
    }
    else if ([((NSString *) nil_chk(OrgXmlpullV1Sax2Driver_NAMESPACE_PREFIXES_FEATURE_)) isEqual:name]) {
      if ([((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getFeatureWithNSString:[OrgXmlpullV1XmlPullParser FEATURE_REPORT_NAMESPACE_ATTRIBUTES]] != value) {
        [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) setFeatureWithNSString:[OrgXmlpullV1XmlPullParser FEATURE_REPORT_NAMESPACE_ATTRIBUTES] withBOOL:value];
      }
    }
    else if ([((NSString *) nil_chk(OrgXmlpullV1Sax2Driver_VALIDATION_FEATURE_)) isEqual:name]) {
      [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) setFeatureWithNSString:[OrgXmlpullV1XmlPullParser FEATURE_VALIDATION] withBOOL:value];
    }
    else {
      [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) setFeatureWithNSString:name withBOOL:value];
    }
  }
  @catch (OrgXmlpullV1XmlPullParserException *ex) {
  }
}

- (id)getPropertyWithNSString:(NSString *)name {
  if ([((NSString *) nil_chk(OrgXmlpullV1Sax2Driver_DECLARATION_HANDLER_PROPERTY_)) isEqual:name]) {
    return nil;
  }
  else if ([((NSString *) nil_chk(OrgXmlpullV1Sax2Driver_LEXICAL_HANDLER_PROPERTY_)) isEqual:name]) {
    return nil;
  }
  else {
    return [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getPropertyWithNSString:name];
  }
}

- (void)setPropertyWithNSString:(NSString *)name
                         withId:(id)value {
  if ([((NSString *) nil_chk(OrgXmlpullV1Sax2Driver_DECLARATION_HANDLER_PROPERTY_)) isEqual:name]) {
    @throw [[[OrgXmlSaxSAXNotSupportedException alloc] initWithNSString:[NSString stringWithFormat:@"not supported setting property %@", name]] autorelease];
  }
  else if ([((NSString *) nil_chk(OrgXmlpullV1Sax2Driver_LEXICAL_HANDLER_PROPERTY_)) isEqual:name]) {
    @throw [[[OrgXmlSaxSAXNotSupportedException alloc] initWithNSString:[NSString stringWithFormat:@"not supported setting property %@", name]] autorelease];
  }
  else {
    @try {
      [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) setPropertyWithNSString:name withId:value];
    }
    @catch (OrgXmlpullV1XmlPullParserException *ex) {
      @throw [[[OrgXmlSaxSAXNotSupportedException alloc] initWithNSString:[NSString stringWithFormat:@"not supported set property %@: %@", name, ex]] autorelease];
    }
  }
}

- (void)setEntityResolverWithOrgXmlSaxEntityResolver:(id<OrgXmlSaxEntityResolver>)resolver {
}

- (id<OrgXmlSaxEntityResolver>)getEntityResolver {
  return nil;
}

- (void)setDTDHandlerWithOrgXmlSaxDTDHandler:(id<OrgXmlSaxDTDHandler>)handler {
}

- (id<OrgXmlSaxDTDHandler>)getDTDHandler {
  return nil;
}

- (void)setContentHandlerWithOrgXmlSaxContentHandler:(id<OrgXmlSaxContentHandler>)handler {
  self.contentHandler = handler;
}

- (id<OrgXmlSaxContentHandler>)getContentHandler {
  return contentHandler_;
}

- (void)setErrorHandlerWithOrgXmlSaxErrorHandler:(id<OrgXmlSaxErrorHandler>)handler {
  self.errorHandler = handler;
}

- (id<OrgXmlSaxErrorHandler>)getErrorHandler {
  return errorHandler_;
}

- (void)parseWithOrgXmlSaxInputSource:(OrgXmlSaxInputSource *)source {
  JreOperatorRetainedAssign(&systemId_, self, [((OrgXmlSaxInputSource *) nil_chk(source)) getSystemId]);
  [((id<OrgXmlSaxContentHandler>) nil_chk(contentHandler_)) setDocumentLocatorWithOrgXmlSaxLocator:self];
  JavaIoReader *reader = [((OrgXmlSaxInputSource *) nil_chk(source)) getCharacterStream];
  @try {
    if (reader == nil) {
      JavaIoInputStream *stream = [((OrgXmlSaxInputSource *) nil_chk(source)) getByteStream];
      NSString *encoding = [((OrgXmlSaxInputSource *) nil_chk(source)) getEncoding];
      if (stream == nil) {
        JreOperatorRetainedAssign(&systemId_, self, [((OrgXmlSaxInputSource *) nil_chk(source)) getSystemId]);
        if (systemId_ == nil) {
          OrgXmlSaxSAXParseException *saxException = [[[OrgXmlSaxSAXParseException alloc] initWithNSString:@"null source systemId" withOrgXmlSaxLocator:self] autorelease];
          [((id<OrgXmlSaxErrorHandler>) nil_chk(errorHandler_)) fatalErrorWithOrgXmlSaxSAXParseException:saxException];
          return;
        }
        @try {
          stream = [[[JavaIoFileInputStream alloc] initWithNSString:systemId_] autorelease];
        }
        @catch (JavaIoFileNotFoundException *fnfe) {
          OrgXmlSaxSAXParseException *saxException = [[[OrgXmlSaxSAXParseException alloc] initWithNSString:[NSString stringWithFormat:@"could not open file with systemId %@", systemId_] withOrgXmlSaxLocator:self withJavaLangException:fnfe] autorelease];
          [((id<OrgXmlSaxErrorHandler>) nil_chk(errorHandler_)) fatalErrorWithOrgXmlSaxSAXParseException:saxException];
          return;
        }
      }
      [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) setInputWithJavaIoInputStream:stream withNSString:encoding];
    }
    else {
      [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) setInputWithJavaIoReader:reader];
    }
  }
  @catch (OrgXmlpullV1XmlPullParserException *ex) {
    OrgXmlSaxSAXParseException *saxException = [[[OrgXmlSaxSAXParseException alloc] initWithNSString:[NSString stringWithFormat:@"parsing initialization error: %@", ex] withOrgXmlSaxLocator:self withJavaLangException:ex] autorelease];
    [((id<OrgXmlSaxErrorHandler>) nil_chk(errorHandler_)) fatalErrorWithOrgXmlSaxSAXParseException:saxException];
    return;
  }
  @try {
    [((id<OrgXmlSaxContentHandler>) nil_chk(contentHandler_)) startDocument];
    [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) next];
    if ([((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getEventType] != OrgXmlpullV1XmlPullParser_START_TAG) {
      OrgXmlSaxSAXParseException *saxException = [[[OrgXmlSaxSAXParseException alloc] initWithNSString:[NSString stringWithFormat:@"expected start tag not%@", [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp_)) getPositionDescription]] withOrgXmlSaxLocator:self] autorelease];
      [((id<OrgXmlSaxErrorHandler>) nil_chk(errorHandler_)) fatalErrorWithOrgXmlSaxSAXParseException:saxException];
      return;
    }
  }
  @catch (OrgXmlpullV1XmlPullParserException *ex) {
    OrgXmlSaxSAXParseException *saxException = [[[OrgXmlSaxSAXParseException alloc] initWithNSString:[NSString stringWithFormat:@"parsing initialization error: %@", ex] withOrgXmlSaxLocator:self withJavaLangException:ex] autorelease];
    [((id<OrgXmlSaxErrorHandler>) nil_chk(errorHandler_)) fatalErrorWithOrgXmlSaxSAXParseException:saxException];
    return;
  }
  [self parseSubTreeWithOrgXmlpullV1XmlPullParser:pp_];
  [((id<OrgXmlSaxContentHandler>) nil_chk(contentHandler_)) endDocument];
}

- (void)parseWithNSString:(NSString *)systemId {
  [self parseWithOrgXmlSaxInputSource:[[[OrgXmlSaxInputSource alloc] initWithNSString:systemId] autorelease]];
}

- (void)parseSubTreeWithOrgXmlpullV1XmlPullParser:(id<OrgXmlpullV1XmlPullParser>)pp {
  self.pp = pp;
  BOOL namespaceAware = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getFeatureWithNSString:[OrgXmlpullV1XmlPullParser FEATURE_PROCESS_NAMESPACES]];
  @try {
    if ([((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getEventType] != OrgXmlpullV1XmlPullParser_START_TAG) {
      @throw [[[OrgXmlSaxSAXException alloc] initWithNSString:[NSString stringWithFormat:@"start tag must be read before skiping subtree%@", [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getPositionDescription]]] autorelease];
    }
    IOSIntArray *holderForStartAndLength = [IOSIntArray arrayWithLength:2];
    JavaLangStringBuilder *rawName = [[[JavaLangStringBuilder alloc] initWithInt:16] autorelease];
    NSString *prefix = nil;
    NSString *name = nil;
    int level = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getDepth] - 1;
    int type = OrgXmlpullV1XmlPullParser_START_TAG;
    do {
      switch (type) {
        case OrgXmlpullV1XmlPullParser_START_TAG:
        if (namespaceAware) {
          int depth = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getDepth] - 1;
          int countPrev = (level > depth) ? [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getNamespaceCountWithInt:depth] : 0;
          int count = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getNamespaceCountWithInt:depth + 1];
          for (int i = countPrev; i < count; i++) {
            [((id<OrgXmlSaxContentHandler>) nil_chk(contentHandler_)) startPrefixMappingWithNSString:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getNamespacePrefixWithInt:i] withNSString:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getNamespaceUriWithInt:i]];
          }
          name = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getName];
          prefix = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getPrefix];
          if (prefix != nil) {
            [((JavaLangStringBuilder *) nil_chk(rawName)) setLengthWithInt:0];
            [((JavaLangStringBuilder *) nil_chk(rawName)) appendWithNSString:prefix];
            [((JavaLangStringBuilder *) nil_chk(rawName)) appendWithUnichar:':'];
            [((JavaLangStringBuilder *) nil_chk(rawName)) appendWithNSString:name];
          }
          [self startElementWithNSString:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getNamespace] withNSString:name withNSString:prefix == nil ? name : [((JavaLangStringBuilder *) nil_chk(rawName)) description]];
        }
        else {
          [self startElementWithNSString:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getNamespace] withNSString:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getName] withNSString:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getName]];
        }
        break;
        case OrgXmlpullV1XmlPullParser_TEXT:
        {
          IOSCharArray *chars = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getTextCharactersWithIntArray:holderForStartAndLength];
          [((id<OrgXmlSaxContentHandler>) nil_chk(contentHandler_)) charactersWithCharArray:chars withInt:[((IOSIntArray *) nil_chk(holderForStartAndLength)) intAtIndex:0] withInt:[((IOSIntArray *) nil_chk(holderForStartAndLength)) intAtIndex:1]];
        }
        break;
        case OrgXmlpullV1XmlPullParser_END_TAG:
        if (namespaceAware) {
          name = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getName];
          prefix = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getPrefix];
          if (prefix != nil) {
            [((JavaLangStringBuilder *) nil_chk(rawName)) setLengthWithInt:0];
            [((JavaLangStringBuilder *) nil_chk(rawName)) appendWithNSString:prefix];
            [((JavaLangStringBuilder *) nil_chk(rawName)) appendWithUnichar:':'];
            [((JavaLangStringBuilder *) nil_chk(rawName)) appendWithNSString:name];
          }
          [((id<OrgXmlSaxContentHandler>) nil_chk(contentHandler_)) endElementWithNSString:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getNamespace] withNSString:name withNSString:prefix != nil ? name : [((JavaLangStringBuilder *) nil_chk(rawName)) description]];
          int depth = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getDepth];
          int countPrev = (level > depth) ? [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getNamespaceCountWithInt:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getDepth]] : 0;
          int count = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getNamespaceCountWithInt:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getDepth] - 1];
          for (int i = count - 1; i >= countPrev; i--) {
            [((id<OrgXmlSaxContentHandler>) nil_chk(contentHandler_)) endPrefixMappingWithNSString:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getNamespacePrefixWithInt:i]];
          }
        }
        else {
          [((id<OrgXmlSaxContentHandler>) nil_chk(contentHandler_)) endElementWithNSString:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getNamespace] withNSString:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getName] withNSString:[((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getName]];
        }
        break;
        case OrgXmlpullV1XmlPullParser_END_DOCUMENT:
        goto break_LOOP;
      }
      type = [((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) next];
    }
    while ([((id<OrgXmlpullV1XmlPullParser>) nil_chk(pp)) getDepth] > level);
    break_LOOP: ;
  }
  @catch (OrgXmlpullV1XmlPullParserException *ex) {
    OrgXmlSaxSAXParseException *saxException = [[[OrgXmlSaxSAXParseException alloc] initWithNSString:[NSString stringWithFormat:@"parsing error: %@", ex] withOrgXmlSaxLocator:self withJavaLangException:ex] autorelease];
    [((OrgXmlpullV1XmlPullParserException *) nil_chk(ex)) printStackTrace];
    [((id<OrgXmlSaxErrorHandler>) nil_chk(errorHandler_)) fatalErrorWithOrgXmlSaxSAXParseException:saxException];
  }
}

- (void)startElementWithNSString:(NSString *)namespace_
                    withNSString:(NSString *)localName
                    withNSString:(NSString *)qName {
  [((id<OrgXmlSaxContentHandler>) nil_chk(contentHandler_)) startElementWithNSString:namespace_ withNSString:localName withNSString:qName withOrgXmlSaxAttributes:self];
}

- (void)dealloc {
  JreMemDebugRemove(self);
  JreOperatorRetainedAssign(&pp_, self, nil);
  JreOperatorRetainedAssign(&systemId_, self, nil);
  JreOperatorRetainedAssign(&errorHandler_, self, nil);
  JreOperatorRetainedAssign(&contentHandler_, self, nil);
  [super dealloc];
}

- (void)copyAllPropertiesTo:(id)copy {
  [super copyAllPropertiesTo:copy];
  OrgXmlpullV1Sax2Driver *typedCopy = (OrgXmlpullV1Sax2Driver *) copy;
  typedCopy.contentHandler = contentHandler_;
  typedCopy.errorHandler = errorHandler_;
  typedCopy.systemId = systemId_;
  typedCopy.pp = pp_;
}

- (NSArray *)memDebugStrongReferences {
  NSMutableArray *result =
      [[[super memDebugStrongReferences] mutableCopy] autorelease];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:contentHandler_ name:@"contentHandler"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:errorHandler_ name:@"errorHandler"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:systemId_ name:@"systemId"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:pp_ name:@"pp"]];
  return result;
}

@end
