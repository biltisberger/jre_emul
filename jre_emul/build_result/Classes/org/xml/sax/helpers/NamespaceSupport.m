//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: android/libcore/luni/src/main/java/org/xml/sax/helpers/NamespaceSupport.java
//
//  Created by stha1de on 09.08.13.
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "java/lang/IllegalStateException.h"
#include "java/lang/System.h"
#include "java/util/ArrayList.h"
#include "java/util/Collections.h"
#include "java/util/EmptyStackException.h"
#include "java/util/Enumeration.h"
#include "java/util/Hashtable.h"
#include "java/util/List.h"
#include "org/xml/sax/helpers/NamespaceSupport.h"

@implementation OrgXmlSaxHelpersNamespaceSupport

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlSaxHelpersNamespaceSupport_XMLNS_ name:@"OrgXmlSaxHelpersNamespaceSupport_XMLNS_"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlSaxHelpersNamespaceSupport_NSDECL_ name:@"OrgXmlSaxHelpersNamespaceSupport_NSDECL_"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlSaxHelpersNamespaceSupport_EMPTY_ENUMERATION_ name:@"OrgXmlSaxHelpersNamespaceSupport_EMPTY_ENUMERATION_"]];
  return result;
}

static NSString * OrgXmlSaxHelpersNamespaceSupport_XMLNS_ = @"http://www.w3.org/XML/1998/namespace";
static NSString * OrgXmlSaxHelpersNamespaceSupport_NSDECL_ = @"http://www.w3.org/xmlns/2000/";
static id<JavaUtilEnumeration> OrgXmlSaxHelpersNamespaceSupport_EMPTY_ENUMERATION_;

- (IOSObjectArray *)contexts {
  return contexts_;
}
- (void)setContexts:(IOSObjectArray *)contexts {
  JreOperatorRetainedAssign(&contexts_, self, contexts);
}
@synthesize contexts = contexts_;
- (OrgXmlSaxHelpersNamespaceSupport_Context *)currentContext {
  return currentContext_;
}
- (void)setCurrentContext:(OrgXmlSaxHelpersNamespaceSupport_Context *)currentContext {
  JreOperatorRetainedAssign(&currentContext_, self, currentContext);
}
@synthesize currentContext = currentContext_;
@synthesize contextPos = contextPos_;
@synthesize namespaceDeclUris = namespaceDeclUris_;

+ (NSString *)XMLNS {
  return OrgXmlSaxHelpersNamespaceSupport_XMLNS_;
}

+ (NSString *)NSDECL {
  return OrgXmlSaxHelpersNamespaceSupport_NSDECL_;
}

+ (id<JavaUtilEnumeration>)EMPTY_ENUMERATION {
  return OrgXmlSaxHelpersNamespaceSupport_EMPTY_ENUMERATION_;
}

- (id)init {
  if ((self = [super init])) {
    [self reset];
    JreMemDebugAdd(self);
  }
  return self;
}

- (void)reset {
  JreOperatorRetainedAssign(&contexts_, self, [IOSObjectArray arrayWithLength:32 type:[IOSClass classWithClass:[OrgXmlSaxHelpersNamespaceSupport_Context class]]]);
  namespaceDeclUris_ = NO;
  contextPos_ = 0;
  [((IOSObjectArray *) nil_chk(contexts_)) replaceObjectAtIndex:contextPos_ withObject:JreOperatorRetainedAssign(&currentContext_, self, [[[OrgXmlSaxHelpersNamespaceSupport_Context alloc] initWithOrgXmlSaxHelpersNamespaceSupport:self] autorelease])];
  [((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(currentContext_)) declarePrefixWithNSString:@"xml" withNSString:OrgXmlSaxHelpersNamespaceSupport_XMLNS_];
}

- (void)pushContext {
  int max = (int) [((IOSObjectArray *) nil_chk(contexts_)) count];
  ((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk([((IOSObjectArray *) nil_chk(contexts_)) objectAtIndex:contextPos_])).declsOK = NO;
  contextPos_++;
  if (contextPos_ >= max) {
    IOSObjectArray *newContexts = [IOSObjectArray arrayWithLength:max * 2 type:[IOSClass classWithClass:[OrgXmlSaxHelpersNamespaceSupport_Context class]]];
    [JavaLangSystem arraycopyWithId:contexts_ withInt:0 withId:newContexts withInt:0 withInt:max];
    max *= 2;
    JreOperatorRetainedAssign(&contexts_, self, newContexts);
  }
  JreOperatorRetainedAssign(&currentContext_, self, [((IOSObjectArray *) nil_chk(contexts_)) objectAtIndex:contextPos_]);
  if (currentContext_ == nil) {
    [((IOSObjectArray *) nil_chk(contexts_)) replaceObjectAtIndex:contextPos_ withObject:JreOperatorRetainedAssign(&currentContext_, self, [[[OrgXmlSaxHelpersNamespaceSupport_Context alloc] initWithOrgXmlSaxHelpersNamespaceSupport:self] autorelease])];
  }
  if (contextPos_ > 0) {
    [((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(currentContext_)) setParentWithOrgXmlSaxHelpersNamespaceSupport_Context:[((IOSObjectArray *) nil_chk(contexts_)) objectAtIndex:contextPos_ - 1]];
  }
}

- (void)popContext {
  [((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk([((IOSObjectArray *) nil_chk(contexts_)) objectAtIndex:contextPos_])) clear];
  contextPos_--;
  if (contextPos_ < 0) {
    @throw [[[JavaUtilEmptyStackException alloc] init] autorelease];
  }
  JreOperatorRetainedAssign(&currentContext_, self, [((IOSObjectArray *) nil_chk(contexts_)) objectAtIndex:contextPos_]);
}

- (BOOL)declarePrefixWithNSString:(NSString *)prefix
                     withNSString:(NSString *)uri {
  if ([((NSString *) nil_chk(prefix)) isEqual:@"xml"] || [((NSString *) nil_chk(prefix)) isEqual:@"xmlns"]) {
    return NO;
  }
  else {
    [((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(currentContext_)) declarePrefixWithNSString:prefix withNSString:uri];
    return YES;
  }
}

- (IOSObjectArray *)processNameWithNSString:(NSString *)qName
                          withNSStringArray:(IOSObjectArray *)parts
                                   withBOOL:(BOOL)isAttribute {
  IOSObjectArray *myParts = [((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(currentContext_)) processNameWithNSString:qName withBOOL:isAttribute];
  if (myParts == nil) {
    return nil;
  }
  else {
    [((IOSObjectArray *) nil_chk(parts)) replaceObjectAtIndex:0 withObject:[myParts objectAtIndex:0]];
    [((IOSObjectArray *) nil_chk(parts)) replaceObjectAtIndex:1 withObject:[myParts objectAtIndex:1]];
    [((IOSObjectArray *) nil_chk(parts)) replaceObjectAtIndex:2 withObject:[myParts objectAtIndex:2]];
    return parts;
  }
}

- (NSString *)getURIWithNSString:(NSString *)prefix {
  return [((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(currentContext_)) getURIWithNSString:prefix];
}

- (id<JavaUtilEnumeration>)getPrefixes {
  return [((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(currentContext_)) getPrefixes];
}

- (NSString *)getPrefixWithNSString:(NSString *)uri {
  return [((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(currentContext_)) getPrefixWithNSString:uri];
}

- (id<JavaUtilEnumeration>)getPrefixesWithNSString:(NSString *)uri {
  JavaUtilArrayList *prefixes = [[[JavaUtilArrayList alloc] init] autorelease];
  id<JavaUtilEnumeration> allPrefixes = [self getPrefixes];
  while ([((id<JavaUtilEnumeration>) nil_chk(allPrefixes)) hasMoreElements]) {
    NSString *prefix = (NSString *) [((id<JavaUtilEnumeration>) nil_chk(allPrefixes)) nextElement];
    if ([((NSString *) nil_chk(uri)) isEqual:[self getURIWithNSString:prefix]]) {
      [((JavaUtilArrayList *) nil_chk(prefixes)) addWithId:prefix];
    }
  }
  return [JavaUtilCollections enumerationWithJavaUtilCollection:prefixes];
}

- (id<JavaUtilEnumeration>)getDeclaredPrefixes {
  return [((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(currentContext_)) getDeclaredPrefixes];
}

- (void)setNamespaceDeclUrisWithBOOL:(BOOL)value {
  if (contextPos_ != 0) @throw [[[JavaLangIllegalStateException alloc] init] autorelease];
  if (value == namespaceDeclUris_) return;
  namespaceDeclUris_ = value;
  if (value) [((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(currentContext_)) declarePrefixWithNSString:@"xmlns" withNSString:OrgXmlSaxHelpersNamespaceSupport_NSDECL_];
  else {
    [((IOSObjectArray *) nil_chk(contexts_)) replaceObjectAtIndex:contextPos_ withObject:JreOperatorRetainedAssign(&currentContext_, self, [[[OrgXmlSaxHelpersNamespaceSupport_Context alloc] initWithOrgXmlSaxHelpersNamespaceSupport:self] autorelease])];
    [((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(currentContext_)) declarePrefixWithNSString:@"xml" withNSString:OrgXmlSaxHelpersNamespaceSupport_XMLNS_];
  }
}

- (BOOL)isNamespaceDeclUris {
  return namespaceDeclUris_;
}

+ (void)initialize {
  if (self == [OrgXmlSaxHelpersNamespaceSupport class]) {
    JreOperatorRetainedAssign(&OrgXmlSaxHelpersNamespaceSupport_EMPTY_ENUMERATION_, self, [JavaUtilCollections enumerationWithJavaUtilCollection:[JavaUtilCollections emptyList]]);
  }
}

- (void)dealloc {
  JreMemDebugRemove(self);
  JreOperatorRetainedAssign(&currentContext_, self, nil);
  JreOperatorRetainedAssign(&contexts_, self, nil);
  [super dealloc];
}

- (void)copyAllPropertiesTo:(id)copy {
  [super copyAllPropertiesTo:copy];
  OrgXmlSaxHelpersNamespaceSupport *typedCopy = (OrgXmlSaxHelpersNamespaceSupport *) copy;
  typedCopy.contexts = contexts_;
  typedCopy.currentContext = currentContext_;
  typedCopy.contextPos = contextPos_;
  typedCopy.namespaceDeclUris = namespaceDeclUris_;
}

- (NSArray *)memDebugStrongReferences {
  NSMutableArray *result =
      [[[super memDebugStrongReferences] mutableCopy] autorelease];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:contexts_ name:@"contexts"]];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:currentContext_ name:@"currentContext"]];
  return result;
}

@end
@implementation OrgXmlSaxHelpersNamespaceSupport_Context

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  return result;
}

- (OrgXmlSaxHelpersNamespaceSupport *)this$0 {
  return this$0_;
}
@synthesize this$0 = this$0_;
- (JavaUtilHashtable *)prefixTable {
  return prefixTable_;
}
- (void)setPrefixTable:(JavaUtilHashtable *)prefixTable {
  JreOperatorRetainedAssign(&prefixTable_, self, prefixTable);
}
@synthesize prefixTable = prefixTable_;
- (JavaUtilHashtable *)uriTable {
  return uriTable_;
}
- (void)setUriTable:(JavaUtilHashtable *)uriTable {
  JreOperatorRetainedAssign(&uriTable_, self, uriTable);
}
@synthesize uriTable = uriTable_;
- (JavaUtilHashtable *)elementNameTable {
  return elementNameTable_;
}
- (void)setElementNameTable:(JavaUtilHashtable *)elementNameTable {
  JreOperatorRetainedAssign(&elementNameTable_, self, elementNameTable);
}
@synthesize elementNameTable = elementNameTable_;
- (JavaUtilHashtable *)attributeNameTable {
  return attributeNameTable_;
}
- (void)setAttributeNameTable:(JavaUtilHashtable *)attributeNameTable {
  JreOperatorRetainedAssign(&attributeNameTable_, self, attributeNameTable);
}
@synthesize attributeNameTable = attributeNameTable_;
- (NSString *)defaultNS {
  return defaultNS_;
}
- (void)setDefaultNS:(NSString *)defaultNS {
  JreOperatorRetainedAssign(&defaultNS_, self, defaultNS);
}
@synthesize defaultNS = defaultNS_;
@synthesize declsOK = declsOK_;
- (JavaUtilArrayList *)declarations {
  return declarations_;
}
- (void)setDeclarations:(JavaUtilArrayList *)declarations {
  JreOperatorRetainedAssign(&declarations_, self, declarations);
}
@synthesize declarations = declarations_;
@synthesize declSeen = declSeen_;
- (OrgXmlSaxHelpersNamespaceSupport_Context *)parent {
  return parent_;
}
- (void)setParent:(OrgXmlSaxHelpersNamespaceSupport_Context *)parent {
  JreOperatorRetainedAssign(&parent_, self, parent);
}
@synthesize parent = parent_;

- (id)initWithOrgXmlSaxHelpersNamespaceSupport:(OrgXmlSaxHelpersNamespaceSupport *)outer$ {
  if ((self = [super init])) {
    this$0_ = outer$;
    JreOperatorRetainedAssign(&defaultNS_, self, nil);
    declsOK_ = YES;
    JreOperatorRetainedAssign(&declarations_, self, nil);
    declSeen_ = NO;
    JreOperatorRetainedAssign(&parent_, self, nil);
    [self copyTables];
    JreMemDebugAdd(self);
  }
  return self;
}

- (void)setParentWithOrgXmlSaxHelpersNamespaceSupport_Context:(OrgXmlSaxHelpersNamespaceSupport_Context *)parent {
  self.parent = parent;
  JreOperatorRetainedAssign(&declarations_, self, nil);
  JreOperatorRetainedAssign(&prefixTable_, self, ((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(parent)).prefixTable);
  JreOperatorRetainedAssign(&uriTable_, self, ((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(parent)).uriTable);
  JreOperatorRetainedAssign(&elementNameTable_, self, ((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(parent)).elementNameTable);
  JreOperatorRetainedAssign(&attributeNameTable_, self, ((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(parent)).attributeNameTable);
  JreOperatorRetainedAssign(&defaultNS_, self, ((OrgXmlSaxHelpersNamespaceSupport_Context *) nil_chk(parent)).defaultNS);
  declSeen_ = NO;
  declsOK_ = YES;
}

- (void)clear {
  JreOperatorRetainedAssign(&parent_, self, nil);
  JreOperatorRetainedAssign(&prefixTable_, self, nil);
  JreOperatorRetainedAssign(&uriTable_, self, nil);
  JreOperatorRetainedAssign(&elementNameTable_, self, nil);
  JreOperatorRetainedAssign(&attributeNameTable_, self, nil);
  JreOperatorRetainedAssign(&defaultNS_, self, nil);
}

- (void)declarePrefixWithNSString:(NSString *)prefix
                     withNSString:(NSString *)uri {
  if (!declsOK_) {
    @throw [[[JavaLangIllegalStateException alloc] initWithNSString:@"can't declare any more prefixes in this context"] autorelease];
  }
  if (!declSeen_) {
    [self copyTables];
  }
  if (declarations_ == nil) {
    JreOperatorRetainedAssign(&declarations_, self, [[[JavaUtilArrayList alloc] init] autorelease]);
  }
  prefix = [((NSString *) nil_chk(prefix)) intern];
  uri = [((NSString *) nil_chk(uri)) intern];
  if ([@"" isEqual:prefix]) {
    if ([@"" isEqual:uri]) {
      JreOperatorRetainedAssign(&defaultNS_, self, nil);
    }
    else {
      JreOperatorRetainedAssign(&defaultNS_, self, uri);
    }
  }
  else {
    [((JavaUtilHashtable *) nil_chk(prefixTable_)) putWithId:prefix withId:uri];
    [((JavaUtilHashtable *) nil_chk(uriTable_)) putWithId:uri withId:prefix];
  }
  [((JavaUtilArrayList *) nil_chk(declarations_)) addWithId:prefix];
}

- (IOSObjectArray *)processNameWithNSString:(NSString *)qName
                                   withBOOL:(BOOL)isAttribute {
  IOSObjectArray *name;
  JavaUtilHashtable *table;
  declsOK_ = NO;
  if (isAttribute) {
    table = attributeNameTable_;
  }
  else {
    table = elementNameTable_;
  }
  name = (IOSObjectArray *) [((JavaUtilHashtable *) nil_chk(table)) getWithId:qName];
  if (name != nil) {
    return name;
  }
  name = [IOSObjectArray arrayWithLength:3 type:[IOSClass classWithClass:[NSString class]]];
  [((IOSObjectArray *) nil_chk(name)) replaceObjectAtIndex:2 withObject:[((NSString *) nil_chk(qName)) intern]];
  int index = [((NSString *) nil_chk(qName)) indexOf:':'];
  if (index == -1) {
    if (isAttribute) {
      if ([@"xmlns" isEqual:qName] && this$0_.namespaceDeclUris) [((IOSObjectArray *) nil_chk(name)) replaceObjectAtIndex:0 withObject:[OrgXmlSaxHelpersNamespaceSupport NSDECL]];
      else [((IOSObjectArray *) nil_chk(name)) replaceObjectAtIndex:0 withObject:@""];
    }
    else if (defaultNS_ == nil) {
      [((IOSObjectArray *) nil_chk(name)) replaceObjectAtIndex:0 withObject:@""];
    }
    else {
      [((IOSObjectArray *) nil_chk(name)) replaceObjectAtIndex:0 withObject:defaultNS_];
    }
    [((IOSObjectArray *) nil_chk(name)) replaceObjectAtIndex:1 withObject:[((IOSObjectArray *) nil_chk(name)) objectAtIndex:2]];
  }
  else {
    NSString *prefix = [((NSString *) nil_chk(qName)) substring:0 endIndex:index];
    NSString *local = [((NSString *) nil_chk(qName)) substring:index + 1];
    NSString *uri;
    if ([@"" isEqual:prefix]) {
      uri = defaultNS_;
    }
    else {
      uri = (NSString *) [((JavaUtilHashtable *) nil_chk(prefixTable_)) getWithId:prefix];
    }
    if (uri == nil || (!isAttribute && [@"xmlns" isEqual:prefix])) {
      return nil;
    }
    [((IOSObjectArray *) nil_chk(name)) replaceObjectAtIndex:0 withObject:uri];
    [((IOSObjectArray *) nil_chk(name)) replaceObjectAtIndex:1 withObject:[((NSString *) nil_chk(local)) intern]];
  }
  [((JavaUtilHashtable *) nil_chk(table)) putWithId:[((IOSObjectArray *) nil_chk(name)) objectAtIndex:2] withId:name];
  return name;
}

- (NSString *)getURIWithNSString:(NSString *)prefix {
  if ([@"" isEqual:prefix]) {
    return defaultNS_;
  }
  else if (prefixTable_ == nil) {
    return nil;
  }
  else {
    return (NSString *) [prefixTable_ getWithId:prefix];
  }
}

- (NSString *)getPrefixWithNSString:(NSString *)uri {
  if (uriTable_ == nil) {
    return nil;
  }
  else {
    return (NSString *) [uriTable_ getWithId:uri];
  }
}

- (id<JavaUtilEnumeration>)getDeclaredPrefixes {
  return (declarations_ == nil) ? ((id) [OrgXmlSaxHelpersNamespaceSupport EMPTY_ENUMERATION]) : ((id) [JavaUtilCollections enumerationWithJavaUtilCollection:declarations_]);
}

- (id<JavaUtilEnumeration>)getPrefixes {
  if (prefixTable_ == nil) {
    return [OrgXmlSaxHelpersNamespaceSupport EMPTY_ENUMERATION];
  }
  else {
    return [prefixTable_ keys];
  }
}

- (void)copyTables OBJC_METHOD_FAMILY_NONE {
  if (prefixTable_ != nil) {
    JreOperatorRetainedAssign(&prefixTable_, self, (JavaUtilHashtable *) [prefixTable_ clone]);
  }
  else {
    JreOperatorRetainedAssign(&prefixTable_, self, [[[JavaUtilHashtable alloc] init] autorelease]);
  }
  if (uriTable_ != nil) {
    JreOperatorRetainedAssign(&uriTable_, self, (JavaUtilHashtable *) [uriTable_ clone]);
  }
  else {
    JreOperatorRetainedAssign(&uriTable_, self, [[[JavaUtilHashtable alloc] init] autorelease]);
  }
  JreOperatorRetainedAssign(&elementNameTable_, self, [[[JavaUtilHashtable alloc] init] autorelease]);
  JreOperatorRetainedAssign(&attributeNameTable_, self, [[[JavaUtilHashtable alloc] init] autorelease]);
  declSeen_ = YES;
}

- (void)dealloc {
  JreMemDebugRemove(self);
  JreOperatorRetainedAssign(&parent_, self, nil);
  JreOperatorRetainedAssign(&declarations_, self, nil);
  JreOperatorRetainedAssign(&defaultNS_, self, nil);
  JreOperatorRetainedAssign(&attributeNameTable_, self, nil);
  JreOperatorRetainedAssign(&elementNameTable_, self, nil);
  JreOperatorRetainedAssign(&uriTable_, self, nil);
  JreOperatorRetainedAssign(&prefixTable_, self, nil);
  [super dealloc];
}

- (void)copyAllPropertiesTo:(id)copy {
  [super copyAllPropertiesTo:copy];
  OrgXmlSaxHelpersNamespaceSupport_Context *typedCopy = (OrgXmlSaxHelpersNamespaceSupport_Context *) copy;
  typedCopy.this$0 = this$0_;
  typedCopy.prefixTable = prefixTable_;
  typedCopy.uriTable = uriTable_;
  typedCopy.elementNameTable = elementNameTable_;
  typedCopy.attributeNameTable = attributeNameTable_;
  typedCopy.defaultNS = defaultNS_;
  typedCopy.declsOK = declsOK_;
  typedCopy.declarations = declarations_;
  typedCopy.declSeen = declSeen_;
  typedCopy.parent = parent_;
}

- (NSArray *)memDebugStrongReferences {
  NSMutableArray *result =
      [[[super memDebugStrongReferences] mutableCopy] autorelease];
  return result;
}

@end
