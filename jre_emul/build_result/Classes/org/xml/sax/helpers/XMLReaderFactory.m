//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: android/libcore/luni/src/main/java/org/xml/sax/helpers/XMLReaderFactory.java
//
//  Created by stha1de on 09.08.13.
//

#include "java/lang/ClassCastException.h"
#include "java/lang/ClassNotFoundException.h"
#include "java/lang/Exception.h"
#include "java/lang/IllegalAccessException.h"
#include "java/lang/InstantiationException.h"
#include "java/lang/RuntimeException.h"
#include "java/lang/System.h"
#include "org/xml/sax/Parser.h"
#include "org/xml/sax/SAXException.h"
#include "org/xml/sax/XMLReader.h"
#include "org/xml/sax/helpers/NewInstance.h"
#include "org/xml/sax/helpers/ParserAdapter.h"
#include "org/xml/sax/helpers/ParserFactory.h"
#include "org/xml/sax/helpers/XMLReaderFactory.h"

@implementation OrgXmlSaxHelpersXMLReaderFactory

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  [result addObject:[JreMemDebugStrongReference strongReferenceWithObject:OrgXmlSaxHelpersXMLReaderFactory_property_ name:@"OrgXmlSaxHelpersXMLReaderFactory_property_"]];
  return result;
}

static NSString * OrgXmlSaxHelpersXMLReaderFactory_property_ = @"org.xml.sax.driver";

+ (NSString *)property {
  return OrgXmlSaxHelpersXMLReaderFactory_property_;
}

- (id)init {
  return JreMemDebugAdd([super init]);
}

+ (id<OrgXmlSaxXMLReader>)createXMLReader {
  NSString *className_ = nil;
  @try {
    className_ = [JavaLangSystem getPropertyWithNSString:OrgXmlSaxHelpersXMLReaderFactory_property_];
  }
  @catch (JavaLangRuntimeException *e) {
  }
  if (className_ == nil) {
  }
  if (className_ != nil) return [OrgXmlSaxHelpersXMLReaderFactory loadClassWithNSString:className_];
  @try {
    return [[[OrgXmlSaxHelpersParserAdapter alloc] initWithOrgXmlSaxParser:[OrgXmlSaxHelpersParserFactory makeParser]] autorelease];
  }
  @catch (JavaLangException *e) {
    @throw [[[OrgXmlSaxSAXException alloc] initWithNSString:@"Can't create default XMLReader; is system property org.xml.sax.driver set?"] autorelease];
  }
}

+ (id<OrgXmlSaxXMLReader>)createXMLReaderWithNSString:(NSString *)className_ {
  return [OrgXmlSaxHelpersXMLReaderFactory loadClassWithNSString:className_];
}

+ (id<OrgXmlSaxXMLReader>)loadClassWithNSString:(NSString *)className_ {
  @try {
    return (id<OrgXmlSaxXMLReader>) [OrgXmlSaxHelpersNewInstance newInstanceWithNSString:className_];
  }
  @catch (JavaLangClassNotFoundException *e1) {
    @throw [[[OrgXmlSaxSAXException alloc] initWithNSString:[NSString stringWithFormat:@"SAX2 driver class %@ not found", className_] withJavaLangException:e1] autorelease];
  }
  @catch (JavaLangIllegalAccessException *e2) {
    @throw [[[OrgXmlSaxSAXException alloc] initWithNSString:[NSString stringWithFormat:@"SAX2 driver class %@ found but cannot be loaded", className_] withJavaLangException:e2] autorelease];
  }
  @catch (JavaLangInstantiationException *e3) {
    @throw [[[OrgXmlSaxSAXException alloc] initWithNSString:[NSString stringWithFormat:@"SAX2 driver class %@ loaded but cannot be instantiated (no empty public constructor?)", className_] withJavaLangException:e3] autorelease];
  }
  @catch (JavaLangClassCastException *e4) {
    @throw [[[OrgXmlSaxSAXException alloc] initWithNSString:[NSString stringWithFormat:@"SAX2 driver class %@ does not implement XMLReader", className_] withJavaLangException:e4] autorelease];
  }
}

- (void)dealloc {
  JreMemDebugRemove(self);
  [super dealloc];
}

@end
