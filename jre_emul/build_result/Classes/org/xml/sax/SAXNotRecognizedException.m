//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: android/libcore/luni/src/main/java/org/xml/sax/SAXNotRecognizedException.java
//
//  Created by stha1de on 09.08.13.
//

#include "org/xml/sax/SAXNotRecognizedException.h"

@implementation OrgXmlSaxSAXNotRecognizedException

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  return result;
}

- (id)init {
  return JreMemDebugAdd([super init]);
}

- (id)initWithNSString:(NSString *)message {
  return JreMemDebugAdd([super initWithNSString:message]);
}

- (void)dealloc {
  JreMemDebugRemove(self);
  [super dealloc];
}

@end
