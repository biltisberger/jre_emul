//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: android/libcore/luni/src/main/java/javax/xml/parsers/SAXParser.java
//
//  Created by stha1de on 09.08.13.
//

#include "IOSClass.h"
#include "java/io/File.h"
#include "java/io/InputStream.h"
#include "java/io/PrintStream.h"
#include "java/lang/IllegalArgumentException.h"
#include "java/lang/System.h"
#include "java/lang/UnsupportedOperationException.h"
#include "javax/xml/parsers/FilePathToURI.h"
#include "javax/xml/parsers/SAXParser.h"
#include "javax/xml/validation/Schema.h"
#include "org/xml/sax/HandlerBase.h"
#include "org/xml/sax/InputSource.h"
#include "org/xml/sax/Parser.h"
#include "org/xml/sax/XMLReader.h"
#include "org/xml/sax/helpers/DefaultHandler.h"

@implementation JavaxXmlParsersSAXParser

+ (NSArray *)memDebugStaticReferences {
  NSMutableArray *result = [NSMutableArray array];
  return result;
}

- (id)init {
  return JreMemDebugAdd([super init]);
}

- (void)reset {
  @throw [[[JavaLangUnsupportedOperationException alloc] initWithNSString:[NSString stringWithFormat:@"This SAXParser, \"%@\", does not support the reset functionality.", [[self getClass] getName]]] autorelease];
}

- (void)parseWithJavaIoInputStream:(JavaIoInputStream *)is
          withOrgXmlSaxHandlerBase:(OrgXmlSaxHandlerBase *)hb {
  if (is == nil) {
    @throw [[[JavaLangIllegalArgumentException alloc] initWithNSString:@"InputStream cannot be null"] autorelease];
  }
  OrgXmlSaxInputSource *input = [[[OrgXmlSaxInputSource alloc] initWithJavaIoInputStream:is] autorelease];
  [self parseWithOrgXmlSaxInputSource:input withOrgXmlSaxHandlerBase:hb];
}

- (void)parseWithJavaIoInputStream:(JavaIoInputStream *)is
          withOrgXmlSaxHandlerBase:(OrgXmlSaxHandlerBase *)hb
                      withNSString:(NSString *)systemId {
  if (is == nil) {
    @throw [[[JavaLangIllegalArgumentException alloc] initWithNSString:@"InputStream cannot be null"] autorelease];
  }
  OrgXmlSaxInputSource *input = [[[OrgXmlSaxInputSource alloc] initWithJavaIoInputStream:is] autorelease];
  [((OrgXmlSaxInputSource *) nil_chk(input)) setSystemIdWithNSString:systemId];
  [self parseWithOrgXmlSaxInputSource:input withOrgXmlSaxHandlerBase:hb];
}

- (void)parseWithJavaIoInputStream:(JavaIoInputStream *)is
withOrgXmlSaxHelpersDefaultHandler:(OrgXmlSaxHelpersDefaultHandler *)dh {
  if (is == nil) {
    @throw [[[JavaLangIllegalArgumentException alloc] initWithNSString:@"InputStream cannot be null"] autorelease];
  }
  OrgXmlSaxInputSource *input = [[[OrgXmlSaxInputSource alloc] initWithJavaIoInputStream:is] autorelease];
  [self parseWithOrgXmlSaxInputSource:input withOrgXmlSaxHelpersDefaultHandler:dh];
}

- (void)parseWithJavaIoInputStream:(JavaIoInputStream *)is
withOrgXmlSaxHelpersDefaultHandler:(OrgXmlSaxHelpersDefaultHandler *)dh
                      withNSString:(NSString *)systemId {
  if (is == nil) {
    @throw [[[JavaLangIllegalArgumentException alloc] initWithNSString:@"InputStream cannot be null"] autorelease];
  }
  OrgXmlSaxInputSource *input = [[[OrgXmlSaxInputSource alloc] initWithJavaIoInputStream:is] autorelease];
  [((OrgXmlSaxInputSource *) nil_chk(input)) setSystemIdWithNSString:systemId];
  [self parseWithOrgXmlSaxInputSource:input withOrgXmlSaxHelpersDefaultHandler:dh];
}

- (void)parseWithNSString:(NSString *)uri
 withOrgXmlSaxHandlerBase:(OrgXmlSaxHandlerBase *)hb {
  if (uri == nil) {
    @throw [[[JavaLangIllegalArgumentException alloc] initWithNSString:@"uri cannot be null"] autorelease];
  }
  OrgXmlSaxInputSource *input = [[[OrgXmlSaxInputSource alloc] initWithNSString:uri] autorelease];
  [self parseWithOrgXmlSaxInputSource:input withOrgXmlSaxHandlerBase:hb];
}

- (void)parseWithNSString:(NSString *)uri
withOrgXmlSaxHelpersDefaultHandler:(OrgXmlSaxHelpersDefaultHandler *)dh {
  if (uri == nil) {
    @throw [[[JavaLangIllegalArgumentException alloc] initWithNSString:@"uri cannot be null"] autorelease];
  }
  OrgXmlSaxInputSource *input = [[[OrgXmlSaxInputSource alloc] initWithNSString:uri] autorelease];
  [self parseWithOrgXmlSaxInputSource:input withOrgXmlSaxHelpersDefaultHandler:dh];
}

- (void)parseWithJavaIoFile:(JavaIoFile *)f
   withOrgXmlSaxHandlerBase:(OrgXmlSaxHandlerBase *)hb {
  if (f == nil) {
    @throw [[[JavaLangIllegalArgumentException alloc] initWithNSString:@"File cannot be null"] autorelease];
  }
  NSString *escapedURI = [JavaxXmlParsersFilePathToURI filepath2URIWithNSString:[((JavaIoFile *) nil_chk(f)) getAbsolutePath]];
  if (JavaxXmlParsersSAXParser_DEBUG) {
    [((JavaIoPrintStream *) nil_chk([JavaLangSystem out])) printlnWithNSString:[NSString stringWithFormat:@"Escaped URI = %@", escapedURI]];
  }
  OrgXmlSaxInputSource *input = [[[OrgXmlSaxInputSource alloc] initWithNSString:escapedURI] autorelease];
  [self parseWithOrgXmlSaxInputSource:input withOrgXmlSaxHandlerBase:hb];
}

- (void)parseWithJavaIoFile:(JavaIoFile *)f
withOrgXmlSaxHelpersDefaultHandler:(OrgXmlSaxHelpersDefaultHandler *)dh {
  if (f == nil) {
    @throw [[[JavaLangIllegalArgumentException alloc] initWithNSString:@"File cannot be null"] autorelease];
  }
  NSString *escapedURI = [JavaxXmlParsersFilePathToURI filepath2URIWithNSString:[((JavaIoFile *) nil_chk(f)) getAbsolutePath]];
  if (JavaxXmlParsersSAXParser_DEBUG) {
    [((JavaIoPrintStream *) nil_chk([JavaLangSystem out])) printlnWithNSString:[NSString stringWithFormat:@"Escaped URI = %@", escapedURI]];
  }
  OrgXmlSaxInputSource *input = [[[OrgXmlSaxInputSource alloc] initWithNSString:escapedURI] autorelease];
  [self parseWithOrgXmlSaxInputSource:input withOrgXmlSaxHelpersDefaultHandler:dh];
}

- (void)parseWithOrgXmlSaxInputSource:(OrgXmlSaxInputSource *)is
             withOrgXmlSaxHandlerBase:(OrgXmlSaxHandlerBase *)hb {
  if (is == nil) {
    @throw [[[JavaLangIllegalArgumentException alloc] initWithNSString:@"InputSource cannot be null"] autorelease];
  }
  id<OrgXmlSaxParser> parser = [self getParser];
  if (hb != nil) {
    [((id<OrgXmlSaxParser>) nil_chk(parser)) setDocumentHandlerWithOrgXmlSaxDocumentHandler:hb];
    [((id<OrgXmlSaxParser>) nil_chk(parser)) setEntityResolverWithOrgXmlSaxEntityResolver:hb];
    [((id<OrgXmlSaxParser>) nil_chk(parser)) setErrorHandlerWithOrgXmlSaxErrorHandler:hb];
    [((id<OrgXmlSaxParser>) nil_chk(parser)) setDTDHandlerWithOrgXmlSaxDTDHandler:hb];
  }
  [((id<OrgXmlSaxParser>) nil_chk(parser)) parseWithOrgXmlSaxInputSource:is];
}

- (void)parseWithOrgXmlSaxInputSource:(OrgXmlSaxInputSource *)is
   withOrgXmlSaxHelpersDefaultHandler:(OrgXmlSaxHelpersDefaultHandler *)dh {
  if (is == nil) {
    @throw [[[JavaLangIllegalArgumentException alloc] initWithNSString:@"InputSource cannot be null"] autorelease];
  }
  id<OrgXmlSaxXMLReader> reader = [self getXMLReader];
  if (dh != nil) {
    [((id<OrgXmlSaxXMLReader>) nil_chk(reader)) setContentHandlerWithOrgXmlSaxContentHandler:dh];
    [((id<OrgXmlSaxXMLReader>) nil_chk(reader)) setEntityResolverWithOrgXmlSaxEntityResolver:dh];
    [((id<OrgXmlSaxXMLReader>) nil_chk(reader)) setErrorHandlerWithOrgXmlSaxErrorHandler:dh];
    [((id<OrgXmlSaxXMLReader>) nil_chk(reader)) setDTDHandlerWithOrgXmlSaxDTDHandler:dh];
  }
  [((id<OrgXmlSaxXMLReader>) nil_chk(reader)) parseWithOrgXmlSaxInputSource:is];
}

- (id<OrgXmlSaxParser>)getParser {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (id<OrgXmlSaxXMLReader>)getXMLReader {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (BOOL)isNamespaceAware {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (BOOL)isValidating {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (void)setPropertyWithNSString:(NSString *)name
                         withId:(id)value {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
}

- (id)getPropertyWithNSString:(NSString *)name {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (JavaxXmlValidationSchema *)getSchema {
  @throw [[[JavaLangUnsupportedOperationException alloc] initWithNSString:@"This parser does not support specification"] autorelease];
}

- (BOOL)isXIncludeAware {
  @throw [[[JavaLangUnsupportedOperationException alloc] initWithNSString:@"This parser does not support specification"] autorelease];
}

- (void)dealloc {
  JreMemDebugRemove(self);
  [super dealloc];
}

@end
