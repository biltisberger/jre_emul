//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: Classes/java/util/concurrent/Executor.java
//
//  Created by stha1de on 09.08.13.
//

#ifndef _JavaUtilConcurrentExecutor_H_
#define _JavaUtilConcurrentExecutor_H_

@protocol JavaLangRunnable;

#import "JreEmulation.h"

@protocol JavaUtilConcurrentExecutor < NSObject, JavaObject >
- (void)executeWithJavaLangRunnable:(id<JavaLangRunnable>)command;
@end

#endif // _JavaUtilConcurrentExecutor_H_
